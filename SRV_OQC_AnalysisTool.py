import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time
import os
import shutil
import matplotlib.patches as mpatches
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
cwd = os.getcwd()

mark = ["o", "^", "2", "s", "p" , "P", "*", "X", "D"]

font = {'weight' : 'normal',
        'size'   : 16}

plt.rc('font', **font)


def all_files(ext):
    # returns list of all file names in current directory that end with ext
    sampleData = []
    files = os.listdir('.')
    for file in files:
        if file.endswith(ext):
            sampleData.append(file)
    return sampleData
def all_dir():
    # returns list of all dir in cd
    dirs = []
    lsd = os.listdir('.')
    for item in lsd:
        if os.path.isdir(os.path.join(os.path.abspath("."), item)):
            if item[0] == '.':
                continue
            elif item[0] == '_':
                continue
            elif item[0] == 'N':
                continue
            else:
                dirs.append(item)
    return dirs
def classify_sheets():
    sheets = all_files('.csv')
    flow = None
    flow_torque = None
    port0_pd = None
    port5_pd = None
    port11_pd = None
    port16_pd = None
    pd_torque = None
    t1 = None
    t2 = None
    flow_spl = None
    for sheet in sheets:
        split = sheet.split('_')
        segment = split[-1][:-4]
        if segment == 'massflow':
            flow = sheet
            flow_spl = flow.split('_')[1]
        elif segment == 'port0':
            port0_pd = sheet
        elif segment == 'port5.5':
            port5_pd = sheet
        elif segment == 'port11.5':
            port11_pd = sheet
        elif segment == 'port16.5':
            port16_pd = sheet
        elif segment == 'torque':
            if t1 is None:
                t1 = sheet
            else:
                t2 = sheet
        else:
            print("Error - No Match Segment - ")
    if t1 != None:
        if t1.split('_')[1] == flow_spl:
            flow_torque = t1
            pd_torque = t2
        else:
            flow_torque = t2
            pd_torque = t1
    return flow, flow_torque, port0_pd, port5_pd, port11_pd, port16_pd, pd_torque
def find_part_name():
    """Uses folder name to name part, does not override correctly named folders."""
    name = None
    full_path = os.getcwd()
    cur_dir_name = full_path.split('\\')[-1]
    if "Copy" in cur_dir_name:
        raw_number = cur_dir_name.split(" ")[-1]
        number = raw_number.replace('#', "")
        number = number.replace('\\', "")
        number = number.replace("e", "")
        number = number.replace('(', "")
        number = number.replace(" ", "")
        number = number.replace(')', "")
        number = number.replace("y", "")
        number = number.replace("a", "")
        number = number.replace("t", "")
        number = number.replace("/", "")
        #verify then rename below
        lotnum = cur_dir_name.split(" ")[0][:6]
        number = lotnum + number.zfill(3)
        print(number)
    else:
        raw_number = cur_dir_name
        number = raw_number.replace('#', "")#verify so we dont have to rename
    name = number
    return name
def parse_flow(flow):
    try:
        flow_df = pd.read_csv(flow, sep='\t', header=None,
                              names=['Segment', 'Time', 'Time unit', 'Pressure', 'Flow', 'Blank'],
                              error_bad_lines=False)
    except ValueError:
        return None
    flow_df = flow_df.drop(columns='Blank')
    flow_df = flow_df.loc[flow_df['Segment'] != 'Fill  ']
    flow_df = flow_df.loc[flow_df['Segment'] != 'Vent  ']
    flow_df = flow_df.iloc[:-1]
    flow_df = flow_df.reset_index()
    flow_df = flow_df.loc[:, ~flow_df.columns.str.contains('^index')]
    flow_df = flow_df.loc[:, ~flow_df.columns.str.contains('^Time unit')]
    # flow_df['Time'] = pd.to_numeric(flow_df['Time'], downcast='float', errors='coerce')
    # flow_df['Pressure'] = pd.to_numeric(flow_df['Pressure'], downcast='float', errors='coerce')
    # flow_df['Flow'] = pd.to_numeric(flow_df['Flow'], downcast='float', errors='coerce')
    return flow_df
def parse_torque(torque):
    if isinstance(torque, list):
        dfs = []
        for t in torque:
            # print(t)
            flowtorque_df = pd.read_csv(t)
            flowtorque_df = flowtorque_df.loc[:, ~flowtorque_df.columns.str.contains('^Unnamed')]
            dfs.append(flowtorque_df)
        if len(dfs) == 1:
            return dfs[0]
        else:
            p = pd.concat(dfs, axis=1)
            return p
    else:
        flowtorque_df = pd.read_csv(torque)
        flowtorque_df = flowtorque_df.loc[:, ~flowtorque_df.columns.str.contains('^Unnamed')]
        return flowtorque_df
def parse_portPD(port):
    if isinstance(port, list):
        pds = []
        for port0_pd in port:
            port0pd_df = pd.read_csv(port0_pd, sep='\t', header=None,
                                     names=['Segment', 'Time', 'Time unit', 'Pressure', 'Blank'], error_bad_lines=False)
            port0pd_df = port0pd_df.drop(columns='Blank')
            port0pd_df = port0pd_df.loc[port0pd_df['Segment'] != 'Fill  ']
            port0pd_df = port0pd_df.loc[port0pd_df['Segment'] != 'Vent  ']
            port0pd_df = port0pd_df.iloc[:-1]
            # port0pd_df['Time'] = pd.to_numeric(port0pd_df['Time'], downcast='float', errors='coerce')
            # port0pd_df['Pressure'] = pd.to_numeric(port0pd_df['Pressure'], downcast='float', errors='coerce')
            pds.append(port0pd_df)
        if len(pds) == 1:
            return pds[0]
        else:
            p = pd.concat(pds, axis=1)
            return p
    else:
        port0pd_df = pd.read_csv(port, sep='\t', header=None,
                                 names=['Segment', 'Time', 'Time unit', 'Pressure', 'Blank'], error_bad_lines=False)
        port0pd_df = port0pd_df.drop(columns='Blank')
        port0pd_df = port0pd_df.loc[port0pd_df['Segment'] != 'Fill  ']
        port0pd_df = port0pd_df.loc[port0pd_df['Segment'] != 'Vent  ']
        port0pd_df = port0pd_df.iloc[:-1]
        return port0pd_df
def judge_flow(flow_res_df):
    port_max_df = flow_res_df.loc[:, flow_res_df.columns.str.contains("Maximum")]
    port_min_df = flow_res_df.loc[:, flow_res_df.columns.str.contains('Minimum')]
    port_ave_df = flow_res_df.loc[:, flow_res_df.columns.str.contains("Average")]
    num_peaks_df = flow_res_df.loc[:, flow_res_df.columns.str.contains("Number")]
    parts = port_min_df.index
    flow_judge = {}
    for r in range(len(port_max_df.index)):
        j = []
        part_row_max = np.amax(port_max_df.iloc[r, :].values)
        part_row_min = np.amin(port_min_df.iloc[r, :].values)
        part_row_ave = port_ave_df.iloc[r, :].values
        part_row_numpeaks = num_peaks_df.iloc[r, :].values
        if part_row_numpeaks <21:
            j.append("Bad # Flow Peaks")
        elif part_row_numpeaks >21:
            j.append("Bad # Flow Peaks")
        if part_row_min < 70.0:
            if part_row_min == 0:
                j.append("Bad # Flow Peaks")
            else:
                min_ave = np.amin(part_row_ave)
                #print("Min Ave:  %s"%min_ave)
                #print("Part Min:  %s"%part_row_min)
                if min_ave<70.0:
                    j.append("Low Flow")
                    #print("Low Flow:  %s"%port_max_df.index[r])
                else:
                    #print("Avg Above Threshold")
                    #print(port_max_df.index[r])
                    pass
        if part_row_max > 150.0:
            max_ave = np.amax(part_row_ave)
            #print("Max Ave: %s"%max_ave)
            #print("Part Max:  %s"%part_row_max)
            if max_ave >150.0:
                j.append("High Flow")
                print("High Flow:  %s" % port_max_df.index[r])
            else:
                #print("Avg Below Threshold")
                #print(port_max_df.index[r])
                pass
        if len(j) == 0:
            j.append("Pass")
        flow_judge[parts[r]] = "\n".join(j)
    return flow_judge
def judge_torque(torque_stat_df):
    flow_t_df = torque_stat_df.loc[:, torque_stat_df.columns.str.contains("Flow")]
    pd_t_df = torque_stat_df.loc[:, torque_stat_df.columns.str.contains('PD')]
    parts = pd_t_df.index
    tor_judge = {}
    for r in range(len(flow_t_df.index)):
        j = []
        part_row_ft = np.amax(flow_t_df.iloc[r, :].values)
        part_row_pdt = np.amin(pd_t_df.iloc[r, :].values)
        if part_row_ft > 0.25:
            j.append("High Flow Torque")
        if part_row_pdt > 0.25:
            j.append("High PD Torque")
        if len(j) == 0:
            j.append("Pass")
        tor_judge[parts[r]] = j[0]
    return tor_judge
def judge_pdtests(pd_res_df):
    parts = pd_res_df.index
    #print(pd_res_df)
    #print(parts)
    pd_judge = {}
    for r in range(len(parts)):
        #print(r)
        j = []
        pd_res = pd_res_df.iloc[r, :].values
        #print(pd_res)
        res = pd_res[0]
        if res  == 'Hi Pressure':
            j.append("Hi Pressure Repeat Test")
        elif res == 'Low Pressure':
            j.append("Low Pressure Repeat Test")
        elif res == 'Gross Leak':
            j.append("Fail, Gross Leak")
        elif res > 0.2:
            j.append("Fail, Leak")
        else:
            j.append("Pass")
        pd_judge[parts[r]] = j[0]
    return pd_judge
# flow test
def scrape_rotFlow_analysis(summary):
    """Analyzes Rotary Flow Test Data.
    Input:   all_files('.xlsx') return for each RVA part
    Returns:  maximums, avgs for each port.  np arrays len(21)"""
    rot_flow_df = pd.read_excel(summary, sheet_name='Rotary Flow Test', index_col=0)
    time_x = rot_flow_df['Time'].values
    flow_y = rot_flow_df['Flow'].values
    avgs, peaks, mins, num_peaks = avg_peaks(flow_y)
    return num_peaks, peaks, avgs, mins
def avg_peaks(flow_y):
    closed_v = np.where(flow_y < 30)#find where valve is closed
    #print("Closed V")
    #print(closed_v)
    diff = np.ediff1d(closed_v)  #diff between consec elements of array
    #print("Diff closed")
    #print(diff)
    widths = diff[diff > 10]
    #print("Widths")
    #print(widths)#widths of each port (data)
    ports = flow_y[np.nonzero(flow_y >= 30)]
    #print(ports)
    i_left = 0
    i_right = 0
    avgs = np.zeros(21)
    peaks = np.zeros(21)
    mins = np.zeros(21)
    num_peaks = len(widths)
    for i in range(len(widths)):
        i_right += widths[i] - 1
        p = ports[i_left:i_right]
        #print(p)
        #print(p[3:-3])
        avgs[i] = np.mean(p[3:-3])
        peaks[i] = np.amax(p[3:-3])
        mins[i] = np.amin(p[3:-3])
        i_left = i_right
    #print(avgs, peaks, mins)
    return avgs, peaks, mins, num_peaks
# torque tests
def scrape_torqueData(summary, test):
    """
    Analyzes Torque Test Data.
    Input:   all_files('.xlsx') return for each RVA part
            test:  0 if Flow torque, 1 if PD torque
    Returns:
            tor_stats = (max_tor, ave_t)
            jumps = [(time_at_jump, jump_amt), ....]  for jumps > 0.1
            return_ds_time = factor 10 downsampled time arr
            return_ds_tor = factor 10 downsampled torque arr
    """
    if test == 0:
        sheet = 'Rotary Flow Torque'
    elif test == 1:
        sheet = 'Port PD Torque'
    else:
        return 0, 0, 0, 0
    rot_tor_df = pd.read_excel(summary, sheet_name=sheet, index_col=0)
    rot_tor_df.head()
    time_x = rot_tor_df.iloc[:, 0].values
    tor_y = rot_tor_df.iloc[:,1].values

    diff = np.ediff1d(tor_y)
    largest_jumps = diff[diff > 0.01]
    idx_jump = np.nonzero(diff > 0.01)
    time_at_jmp = time_x[idx_jump]
    max_t = np.max(tor_y)
    ave_t = np.mean(tor_y)
    jumps = []
    for t, j in zip(time_at_jmp, largest_jumps):
        jumps.append((t, j))
    tor_stats = (max_t, ave_t)
    return_ds_time = time_x[::10]
    return_ds_tor = tor_y[::10]
    return tor_stats, jumps, return_ds_time, return_ds_tor
def plot_tor_lines(tor_data_df, ax):
    col_num = int(len(tor_data_df.columns))
    for x, t in zip(range(0, col_num, 2), range(1, col_num, 2)):
        ax.plot(tor_data_df.iloc[:,x].values, tor_data_df.iloc[:,t].values)
    return ax
def plot_tordata(flow_tor_plot_df, pd_tor_plot_df):
    # noinspection PyTypeChecker
    comb_tor_fig, comb_tor_ax = plt.subplots(ncols=2,sharey=True, figsize = (20,10))
    flow_tor_plot_df = flow_tor_plot_df.dropna()
    pd_tor_plot_df = pd_tor_plot_df.dropna()
    comb_tor_ax[0] = plot_tor_lines(flow_tor_plot_df, comb_tor_ax[0])
    comb_tor_ax[0].set_ylabel('Max Torque (Nm)')
    comb_tor_ax[0].set_xlabel('Time (s)')
    comb_tor_ax[0].set_title('Flow Test Torque')
    comb_tor_ax[1] = plot_tor_lines(pd_tor_plot_df, comb_tor_ax[1])
    comb_tor_ax[1].set_title('PD Test Torque')
    comb_tor_ax[1].set_xlabel('Time (s)')
    plt.suptitle("SRV Max Torque Results Combined")
    plt.savefig("SRV_Max_T_subplots_full.png")
def plot_tor_scatter(tor_stat_df, batch_ids):
    tor_fig, ax13 = plt.subplots(figsize = (15,10))
    #for b in batch_ids:
        #b_tor_stat_df = tor_stat_df.loc[tor_stat_df.index.str.contains(b),:]
        #pd_tor_v1_df = tor_stat_df.loc[:, pd_tor_plot_df.columns.str.contains(b)]

        #tor_stat_df = tor_stat_df.loc[tor_stat_df['Maximum Torque Flow']!=0]
        #tor_stat_df = tor_stat_df.loc[tor_stat_df['Maximum Torque PD']!=0]
    max_t_flow = tor_stat_df.iloc[:,0].values
    max_t_pd = tor_stat_df.iloc[:,1].values
    #range_x = np.arange(len(max_t_flow))

    ax13.scatter(max_t_flow,max_t_pd, label = batch_ids)
    plt.xlim(-0.020, 0.3)
    plt.ylim(-0.02, 0.3)
    plt.ylabel('Max Torque (Nm) During PD Test')
    plt.xlabel('Max Torque (Nm) During Flow Test')
    plt.title('SRV Max Torque')
    plt.legend(loc=(1.02,0.05),borderaxespad=0)
    plt.subplots_adjust(right=0.7)
    plt.savefig("SRV_Max_T_Combined.png")
# portPD tests
def scrape_pd_data(summary):
    """
    Analyzes PD Test Data.
    Input:   all_files('.xlsx') return for each RVA part
    Returns:
            leak rate for given ports
    """
    port_pd_tests = pd.read_excel(summary, sheet_name='Port PD Tests', index_col=0)
    port_pd_tests.head()

    ports = ['0', '5.5', '11.5', '16.5']
    output = {}
    for i in range(4):
        port0_pd_df=port_pd_tests.loc[:, port_pd_tests.columns.str.contains(ports[i])]
        if port0_pd_df.empty:
            output[ports[i]] = 'Missing Data'
            continue
        pressure_col = port0_pd_df.iloc[:,2].values
        pressure_col = pressure_col[~np.isnan(pressure_col)]
        if len(pressure_col) < 45:
            val = pressure_col[0]
            #print(pressure_col[-1])
            if val<4.0:
                #print("Low P:  %s"%(str(val)))
                output[ports[i]] = 'Low Pressure'
            elif val>5.5:
                #print("High P:  %s"%(str(val)))
                output[ports[i]] = 'Hi Pressure'
            elif pressure_col[-1]<4.0:
                #print("Port %s Gross Leak"%ports[i])
                output[ports[i]] = 'Gross Leak'
            else:
                #print("Short Test, LEAK?")
                settle_zero = port0_pd_df[port0_pd_df[port0_pd_df.columns[0]] == 'Settle'].iloc[:,-1].values[-1]
                test_df = port0_pd_df[port0_pd_df[port0_pd_df.columns[0]] == 'Test  ']
                leak_amt = settle_zero - test_df.iloc[:,-1].values[-1]
                leak_rate = leak_amt / 5.0
                output[ports[i]] = leak_rate
        else:
            settle_zero = port0_pd_df[port0_pd_df[port0_pd_df.columns[0]] == 'Settle'].iloc[:,-1].values[-1]
            test_df = port0_pd_df[port0_pd_df[port0_pd_df.columns[0]] == 'Test  ']
            leak_amt = settle_zero - test_df.iloc[:,-1].values[-1]
            leak_rate = leak_amt / 5.0
            output[ports[i]] = leak_rate
    return output
def flow_plots(flow_df, batch):
    flow_max_df = flow_df.loc[:, flow_df.columns.str.contains('Maximum')]
    #flow_max_df
    flow_ave_df = flow_df.loc[:, flow_df.columns.str.contains('Average')]
    #flow_ave_df
    flow_min_df = flow_df.loc[:, flow_df.columns.str.contains("Minimum")]
    #flow_min_df
    max_flows = np.zeros(21)
    ave_flows = np.zeros_like(max_flows)
    min_flows = np.zeros_like(max_flows)
    range_x = np.arange(1,22)
    vert_err = np.zeros_like(max_flows)
    itt = 0
    for mx, a, mn in zip(np.flip(flow_max_df.columns), np.flip(flow_ave_df.columns), np.flip(flow_min_df.columns)):
        #print(m, a)
        port_mx = flow_max_df[mx]
        port_a = flow_ave_df[a]
        port_mn = flow_min_df[mn]
        try:
            max_flows[itt] = np.nanmax(port_mx)
            ave_flows[itt] = np.nanmean(port_a)
            min_flows[itt] = np.nanmin(port_mn)
            vert_err[itt] = np.nanstd(port_a)
        except ValueError:
            print(port_mx, port_a, port_mn)
        itt+=1
    #print(ave_flows, max_flows, vert_err)
    meta_fig, ax = plt.subplots(figsize = (15,10))
    ax.plot(range_x, max_flows, '-r', label='Maximum Flow')
    ax.plot(range_x, min_flows, '-m', label='Minimum Flow')
    ax.errorbar(range_x, ave_flows, yerr=vert_err, fmt='-k', ecolor='b', capsize=3, label = '# Average Flow\nwith St.Dev.')
    for x,y in zip(range_x, ave_flows):
        label = str(int(y))
        plt.annotate(label, (x,y), textcoords = "offset points", xytext=(0,10), ha='center')
    for x,y in zip(range_x, max_flows):
        label = str(int(y))
        plt.annotate(label, (x,y), textcoords = "offset points", xytext=(0,10), ha='center')
    for x,y in zip(range_x, min_flows):
        label = str(int(y))
        plt.annotate(label, (x,y), textcoords = "offset points", xytext=(0,10), ha='center')
    plt.xticks(np.arange(1, 22, step=1))
    plt.ylabel('Flow (sccm)')
    plt.xlabel('Port #')
    plt.title('Flow through SRV Ports, %s'%batch)
    plt.legend(loc=(1.02,0.8),borderaxespad=0)
    plt.subplots_adjust(right=0.8)
    plt.savefig("SRV_FlowData_%s.png"%batch)
def make_pies(pd_res_df, sup_title):
    #print("Pies Start")
    pd_pie_fig, pd_pies = plt.subplots(ncols=5, figsize = (15,10))
    pd_tot = {}   #col  = valve, idx = [tot tests, numreg, numGL, numHP]
    #for all 4 valves
    for i in range(4):
        valv_vals = pd_res_df.iloc[:, i]
        num_tests = len(valv_vals)
        valve_hp = len(valv_vals.loc[valv_vals == 'Hi Pressure'].values)    #number of hi p
        valve_gl = len(valv_vals.loc[valv_vals == 'Gross Leak'].values)    #number of grosss leak
        valve_pd = valv_vals.loc[valv_vals != 'Gross Leak']
        valve_pd = valve_pd.loc[valve_pd != 'Hi Pressure']
        valve_pd = valve_pd.loc[valve_pd != 'Missing Data']
        pds = len(valve_pd.values)#actual pd tests
        valve_pd_bcpass = len(valve_pd.loc[valve_pd < 0.02].values)   #best case
        valve_pd_pass = len(valve_pd.values[(valve_pd > 0.02) & (valve_pd < 0.2)])   #pass
        valve_test = pd_res_df.columns[i]
        valve_leak = pds - valve_pd_pass -valve_pd_bcpass   #leaks
        pd_tot[valve_test] = [num_tests,pds,valve_leak, valve_pd_pass, valve_pd_bcpass, valve_gl, valve_hp]    #output
    tot_tests = 0
    tot_pd = 0
    tot_leak=0
    tot_p=0
    tot_bc=0
    tot_gl=0
    tot_hp = 0
    #print("pies loop2")
    for i in range(len(pd_res_df.index)):
        point = pd_res_df.iloc[i,:].values
        if "Hi Pressure" in point:
            tot_hp+=1
        elif "Gross Leak" in point:
            tot_gl+=1
        else:
            bc = 0
            p = 0
            l = 0
            for e in point:
                if e <= 0.02:
                    bc+=1
                elif e<=0.2:
                    p+=1
                else:
                    l+=1
            if l>0:
                tot_leak+=1
            elif p>0:
                tot_p+=1
            else:
                tot_bc+=1
    pd_tot['Totals'] = [tot_tests,tot_pd,tot_leak, tot_p, tot_bc, tot_gl, tot_hp]
    tot_pie_df = pd.DataFrame(pd_tot, index = ['Tests', "PD",'L', "PDP", "PDBC", 'GL', 'HP'])
    cols = ['Totals', '0', '5.5', '11.5', '16.5']
    #print("pies loop3")
    for i in range(5):
        tot_fracs = [tot_pie_df[cols[i]]['L'], tot_pie_df[cols[i]]['PDBC'],tot_pie_df[cols[i]]['PDP'], tot_pie_df[cols[i]]['GL'], tot_pie_df[cols[i]]['HP']]
        cats = [str(tot_pie_df[cols[i]]['L'])+"\nL", str(tot_pie_df[cols[i]]['PDBC'])+"\nBC",str(tot_pie_df[cols[i]]['PDP'])+"\nP", str(tot_pie_df[cols[i]]['GL'])+"\nGL", str(tot_pie_df[cols[i]]['HP'])+"\nHP"]
        color_c = ['#E67E22','#27AE60', '#F4D03F','#CB4335', '#8E44AD']
        ex = [0.5,0.1,0.1,0,0]
        rms = []
        for it in range(len(tot_fracs)):
            if tot_fracs[it] == 0:
                rms.append(it)
        if len(rms)>0:
            rms.sort(reverse=True)
            for e in rms:
                del tot_fracs[e]
                del cats[e]
                del color_c[e]
                del ex[e]
        #print(tot_fracs)
        pd_pies[i].pie(tot_fracs, labels = cats, autopct='%.0f%%', textprops={'size':'smaller'}, explode=ex, colors = color_c )
        titl = "Total Parts\nResults" if i==0 else "Valve\n"+cols[i]
        pd_pies[i].set_title(titl)
        ttl = pd_pies[i].title
        ttl.set_position([0.5, 1.1])
    #print("Pies ending")
    l_p = mpatches.Patch(color='#E67E22', label = 'Leak')
    pbc_p = mpatches.Patch(color='#27AE60', label = 'Pass Best Case')
    p_p = mpatches.Patch(color='#F4D03F', label = 'Pass')
    gl_p = mpatches.Patch(color='#CB4335', label = 'Gross Leak')
    hp_p = mpatches.Patch(color='#8E44AD', label = 'Hi Pressure')
    hand = [l_p, pbc_p, p_p, gl_p, hp_p]
    plt.legend(handles = hand,loc=(-4,-0.5),borderaxespad=0, ncol = 5)
    plt.subplots_adjust(top=0.85)
    plt.suptitle(sup_title)
    plt.savefig(sup_title+"_pies.png")
    return tot_pie_df
#
def first_pass_analysis(makemeta):
    """On startup, prob while user reads, checks folders for batches.
      If batch doesnt have batchres, makes one"""
    out_dir = os.getcwd()
    batches = all_dir()
    print("\nFound %s Lots"%len(batches))
    times = []
    meta = Meta()
    for b in batches:
        print(b)
        os.chdir(b)
        t_0 = time.perf_counter()
        excel_ws = all_files(".xlsx")
        print("Excel WS in Lotdir:  %s"%excel_ws)
        if len(excel_ws)>0:
            if 'BatchResult' in excel_ws[0]:
                #batch_results = scrape_batch(excel_ws[0])
                #transfer data up for meta analysis
                meta.add_lot(b, excel_ws[0])
            else:
                print("Misplaced Excel File in Lot Directory")
        else:
            new_lot = Lot()
            num_parts = len(new_lot.parts)
            times.append((time.perf_counter() - t_0)/num_parts)
            # transfer data up for meta analysis
            meta.add_lot(b, new_lot.batchResult)
        os.chdir(out_dir)
    if len(times)>0:
        print("Processed %s Batches. Avg. Process Time per part:  %s"%(len(times), np.mean(times)))
    else:
        print("Cannot Find Any Unprocessed Batches")
    if makemeta:
        with pd.ExcelWriter("SRVOQC_MetaAnalysis.xlsx") as writer:
            meta.metaResult.to_excel(writer, sheet_name='MetaResult')
            meta.flow_df.to_excel(writer, sheet_name='Flow Data')
            meta.pd_rowdict_df.to_excel(writer, sheet_name='PD Result Data')
            meta.tor_stat_df.to_excel(writer, sheet_name='Torque Stat Data')
        writer.save()
        meta.hist_plot_tor()
        meta.hist_plot_flow()
def analyze_specific_batch():
    batches = all_dir()
    for idx, batch_folder in enumerate(batches):
        print(idx, "\t|\t", batch_folder)
    print("99\t|\tExit")
    response = input("\nEnter Number above to analyze a specific batch...\n\n")
    if response == "99":
        print("Exiting...")
        return 1
    else:
        out_dir = os.getcwd()
        try:
            os.chdir(batches[int(response)])
        except IndexError:
            print("Invalid Response!")
            return 0
        print(batches[int(response)])
        t_0 = time.perf_counter()
        new_lot = Lot()
        num_parts = len(new_lot.parts)
        print("Analysis Time:  %s seconds per part"%((time.perf_counter() - t_0) / num_parts))
        os.chdir(out_dir)
    return 0


class Meta:
    def __init__(self):
        self.lots = []
        self.metaResult = pd.DataFrame()
        self.meta_dir = os.getcwd()
        #cumulative data for meta, rowindexed only
        self.flow_df = pd.DataFrame()
        self.tor_stat_df = pd.DataFrame()
        self.pd_rowdict_df = pd.DataFrame(columns=['0', '5.5', '11.5', '16.5'])
    def add_lot(self, lot_name, batch_res_wb):
        dates_df = pd.read_excel(batch_res_wb, sheet_name = 'ResultTable', usecols = [0,1], nrows=1)
        start_date = dates_df.iloc[0,0]
        end_date = dates_df.iloc[0,1]
        meta_result_dict = {}
        meta_result_dict['Name'] = lot_name
        meta_result_dict['Start Date'] = start_date
        meta_result_dict['End Date'] = end_date
        resultTable = pd.read_excel(batch_res_wb, sheet_name = 'ResultTable', index_col = [0], skiprows=[0,1,2,3])
        meta_result_dict['Number Parts'] = len(resultTable)
        #flow results
        flow_results = resultTable['Flow Test Results'].values
        p = 0
        lf = 0
        hf = 0
        bp = 0
        values, counts = np.unique(flow_results, return_counts=True)
        for i in range(len(values)):
            if values[i] == 'Pass':
                p+=counts[i]
            elif values[i] == 'Low Flow':
                lf += counts[i]
            elif values[i] == 'High Flow':
                hf+= counts[i]
            else:
                bp+= counts[i]
        meta_result_dict['Flow Pass'] = p
        #torque results
        tor_results = resultTable['Torque Test Results'].values
        p = 0
        ht_pd = 0
        ht_f = 0
        values, counts = np.unique(tor_results, return_counts=True)
        for i in range(len(values)):
            if values[i] == 'Pass':
                p+=counts[i]
            elif values[i] == 'High Flow Torque':
                ht_f += counts[i]
            elif values[i] == 'High PD Torque':
                ht_pd += counts[i]
            else:
                print("Bad Judge, Torque")
        meta_result_dict['Torque Pass'] = p
        #PD results
        pd_results = resultTable['Leak Test Results'].values
        p = 0
        hp = 0
        lp = 0
        gl = 0
        leak = 0
        values, counts = np.unique(pd_results, return_counts=True)
        for i in range(len(values)):
            if values[i] == 'Pass':
                p+=counts[i]
            elif values[i] == "Hi Pressure Repeat Test":
                hp += counts[i]
            elif values[i] == "Low Pressure Repeat Test":
                lp += counts[i]
            elif values[i] == "Fail, Gross Leak":
                gl += counts[i]
            elif values[i] == "Fail, Leak":
                leak += counts[i]
            else:
                print("Bad Judge, Leaks")
        meta_result_dict['Leak Result Pass'] = p
        meta_result_dict['Low Flows'] = lf
        meta_result_dict['High Flows'] = hf
        meta_result_dict['Bad # Peaks'] = bp
        meta_result_dict['High Torque PD Test'] = ht_pd
        meta_result_dict['High Torque Flow Test'] = ht_f
        meta_result_dict['Leak Test High Pressure'] = hp
        meta_result_dict['Leak Test Low Pressure'] = lp
        meta_result_dict['Leak Test Gross Leak'] = gl
        meta_result_dict['Leak Test Leak'] = leak
        if self.metaResult.empty:
            self.metaResult = pd.DataFrame(meta_result_dict, index=[lot_name])
        else:
            self.metaResult = pd.concat([self.metaResult, pd.DataFrame(meta_result_dict, index=[lot_name])])
        #prob parse resultTable
        if self.flow_df.empty:
            self.flow_df = pd.read_excel(batch_res_wb, sheet_name='Flow Data', index_col=[0])
        else:
            self.flow_df = pd.concat([self.flow_df, pd.read_excel(batch_res_wb, sheet_name='Flow Data', index_col=[0])])
        if self.tor_stat_df.empty:
            self.tor_stat_df = pd.read_excel(batch_res_wb, sheet_name='Torque Stat Data', index_col=[0])
        else:
            self.tor_stat_df = pd.concat([self.tor_stat_df, pd.read_excel(batch_res_wb, sheet_name='Torque Stat Data', index_col=[0])])
        if self.pd_rowdict_df.empty:
            self.pd_rowdict_df = pd.read_excel(batch_res_wb, sheet_name='PD Result Data', index_col=[0])
        else:
            self.pd_rowdict_df = pd.concat([self.pd_rowdict_df, pd.read_excel(batch_res_wb, sheet_name='PD Result Data', index_col=[0])])
        self.lots.append(lot_name)
    def hist_plot_tor(self):
        fig1, ax1 = plt.subplots(figsize=(15, 10))
        binss = np.histogram(np.hstack((self.tor_stat_df.iloc[:, 0], self.tor_stat_df.iloc[:, 1])), bins = 100)[1]
        ax1.hist(self.tor_stat_df.iloc[:, 0], bins=binss, color='mediumslateblue', alpha=0.7, label='Flow Torque')
        ax1.hist(self.tor_stat_df.iloc[:, 1], bins=binss, color='mediumseagreen', alpha=0.7, label='PD Torque')
        plt.legend()
        ax1.set(xlabel='Torque (Nm)', ylabel='Number of Parts')
        plt.suptitle("All Part Torque Tests")
        plt.savefig("MetaTorqueHist.png")
    def hist_plot_flow(self):
        flow_data_avg = self.flow_df.loc[:, self.flow_df.columns.str.contains("Average")]
        n = 21
        colors = plt.cm.nipy_spectral(np.linspace(0, 1, n))
        fig, ax = plt.subplots(figsize=(15, 10))
        for i in range(21):
            ax.hist(flow_data_avg.iloc[:, i], bins=30, color=colors[i], alpha=0.7,
                    label=(" ").join(flow_data_avg.iloc[:, i].name.split(" ")[:2]))
        plt.legend(loc=(1.02, 0.05), borderaxespad=0)
        plt.subplots_adjust(right=0.7)
        ax.set(xlabel='Flow (sccm)', ylabel='Number of Parts')
        plt.suptitle("All Part Flow Tests by Port")
        plt.savefig("MetaFlowTest_byPort.png")
class Lot:
    def __init__(self):
        self.name = os.getcwd().split("\\")[-1]
        self.start_date = None
        self.end_date = None
        self.parts = []
        self.batchResult = None
        self.lot_dir = os.getcwd()
        self.sub_batch = []  #list of subbatch folder names appended to during classify_(), processed at end of classify
        #cumulative data for batchResult
        self.flowave_data_rowdict = {}
        self.flowpeak_data_rowdict = {}
        self.flowmin_data_rowdict = {}
        self.flownumpeaks_data_rowdict = {}
        self.tor_stat_rowdict = {}
        self.flow_df = pd.DataFrame()
        self.tor_stat_df = pd.DataFrame()
        self.flow_tor_plot_df = pd.DataFrame()
        self.pd_tor_plot_df = pd.DataFrame()
        self.pd_rowdict_df = pd.DataFrame(columns=['0', '5.5', '11.5', '16.5'])
        #Most other vars above
        self.classify_lot()

    def add_part(self, part_dir):
        #moves to part directory
        print("New part:  %s"%part_dir)
        os.chdir(part_dir)
        #Creates Part()
        new_part = Part()
        if new_part.summary == 0:
            os.chdir(self.lot_dir)
            try:
                os.rmdir(part_dir)
            except OSError:
                pass
            return 0
        #adds part to parts list attribute
        self.parts.append(new_part)
        #checks Lot.self dates
        if self.start_date == None:
            self.start_date = new_part.date
        elif  self.end_date == None:
            self.end_date =  new_part.date
        elif int(new_part.date) < int(self.start_date):
            self.start_date = new_part.date
        elif int(new_part.date) > int(self.end_date):
            self.end_date = new_part.date
        num_peaks, flow_peaks, flow_ave, flow_min = scrape_rotFlow_analysis(new_part.summary)
        flow_tor_stats, flow_tor_jumps, f_return_ds_time, f_return_ds_tor = scrape_torqueData(new_part.summary, 0)
        pd_dict_out = scrape_pd_data(new_part.summary)
        pd_tor_stats, pd_tor_jumps, pd_return_ds_time, pd_return_ds_tor = scrape_torqueData(new_part.summary, 1)
        ## add to Lot_data
        # flow data
        self.flowave_data_rowdict[new_part.name] = flow_ave
        self.flowpeak_data_rowdict[new_part.name] = flow_peaks
        self.flowmin_data_rowdict[new_part.name] = flow_min
        self.flownumpeaks_data_rowdict[new_part.name] = num_peaks
        # tor data
        self.tor_stat_rowdict[new_part.name] = [flow_tor_stats[0], pd_tor_stats[0]]
        ttime = pd.DataFrame(f_return_ds_time, columns=['Flow Torque Time %s' % new_part.name])
        tor = pd.DataFrame(f_return_ds_tor, columns=['Flow Torque %s' % new_part.name])
        testcols = pd.concat([ttime, tor], axis=1)  ########################
        self.flow_tor_plot_df = pd.concat([self.flow_tor_plot_df, testcols], axis=1)
        # pd tor data
        ttime = pd.DataFrame(pd_return_ds_time, columns=['PD Torque Time %s' % new_part.name])
        tor = pd.DataFrame(pd_return_ds_tor, columns=['PD Torque %s' % new_part.name])
        pd_testcols = pd.concat([ttime, tor], axis=1)  ######################
        self.pd_tor_plot_df = pd.concat([self.pd_tor_plot_df, pd_testcols], axis=1)
        # pd data
        pd_temp_df = pd.DataFrame(pd_dict_out, index=[new_part.name])
        self.pd_rowdict_df = pd.concat([self.pd_rowdict_df, pd_temp_df])
        #moves back to Lot Dir
        os.chdir(self.lot_dir)
        #renames part directory if needed
        if new_part.name != part_dir:
            try:
                os.rename((self.lot_dir+"\\"+part_dir), (self.lot_dir+"\\"+new_part.name))
            except PermissionError:
                pass


    def make_resultfile(self, subs):
        print("Combining Lot Results")
        #make headings and strings
        port = 'Port '
        headings = [port + '%s' % str(i) for i in range(21, 0, -1)]
        max_headings = [h + ' Maximum' for h in headings]
        avg_headings = [h + ' Average' for h in headings]
        min_headings = [h + ' Minimum' for h in headings]
        num_peak_headings = ['Number Flow Peaks']
        tor_headings = ['Maximum Torque Flow', 'Maximum Torque PD']
        #combine dictionaries, make dfs, combine dfs, etc
        if subs == False:
            self.tor_stat_df = pd.DataFrame.from_dict(self.tor_stat_rowdict, orient='index', columns=tor_headings)
            flowave_df = pd.DataFrame.from_dict(self.flowave_data_rowdict, orient='index', columns=avg_headings)
            flowpeak_df = pd.DataFrame.from_dict(self.flowpeak_data_rowdict, orient='index', columns=max_headings)
            flowmin_df = pd.DataFrame.from_dict(self.flowmin_data_rowdict, orient='index', columns=min_headings)
            flownumpeaks_df = pd.DataFrame.from_dict(self.flownumpeaks_data_rowdict, orient='index', columns=num_peak_headings)
            self.flow_df = pd.concat([flowave_df, flowpeak_df, flowmin_df, flownumpeaks_df], axis=1)
        #make batchresult
        self.batchResult = '%s_BatchResult.xlsx' % self.name
        part_res_df = self.plot_results()
        with pd.ExcelWriter(self.batchResult) as writer:
            part_res_df.to_excel(writer, sheet_name='ResultTable', startrow=4, startcol=0)
            self.flow_df.to_excel(writer, sheet_name='Flow Data')
            self.pd_rowdict_df.to_excel(writer, sheet_name='PD Result Data')
            self.tor_stat_df.to_excel(writer, sheet_name='Torque Stat Data')
            self.flow_tor_plot_df.to_excel(writer, sheet_name='Flow Torque Plot Data')
            self.pd_tor_plot_df.to_excel(writer, sheet_name='PD Torque Plot Data')
            worksheet = writer.sheets['ResultTable']
            worksheet.write_string(0,0,"Start Date:")
            worksheet.write_string(1, 0, self.start_date)
            worksheet.write_string(0,1, "End Date:")
            worksheet.write_string(1, 1, self.end_date)
        print("Done.")
        writer.save()

    def plot_results(self):
        print("Plotting.")
        flowplots = flow_plots(self.flow_df, self.name)
        pies = make_pies(self.pd_rowdict_df, self.name)
        tor_scatter = plot_tor_scatter(self.tor_stat_df, self.name)
        tordat = plot_tordata(self.flow_tor_plot_df, self.pd_tor_plot_df)
        flow_res = judge_flow(self.flow_df)
        flow_res_df = pd.DataFrame.from_dict(flow_res, orient='index', columns=["Flow Test Results"])
        torque_res = judge_torque(self.tor_stat_df)
        torque_res_df = pd.DataFrame.from_dict(torque_res, orient='index', columns=["Torque Test Results"])
        pd_res = judge_pdtests(self.pd_rowdict_df)
        pd_res_df = pd.DataFrame.from_dict(pd_res, orient='index', columns=["Leak Test Results"])
        part_result_df = pd.concat([flow_res_df, torque_res_df, pd_res_df], axis=1)
        if len(part_result_df.index) <= 5:
            res = self.make_result_table(part_result_df)
        plot_path = self.lot_dir + "\\ResultPlots"  # moves .png plots to ResultPlots folder (makes if not present)
        try:
            os.mkdir(plot_path)
        except FileExistsError:
            pass
        pngs = all_files(".png")
        for png in pngs:
            shutil.move(png, plot_path + "\\" + png)
        return part_result_df

    def make_result_table(self, part_result_df):
        new_rows = []
        rows = part_result_df.index.values
        e = "_"
        for r in rows:
            new_rows.append(e.join([self.name, r]))
        columns = part_result_df.columns.values
        res_colors = []
        for row_res in part_result_df.values:
            row_color = []
            for r in row_res:
                if r == "Pass":
                    row_color.append("#73AC57")
                elif r == "High Flow Torque":
                    row_color.append("#FB0B30")
                elif r == "High PD Torque":
                    row_color.append("#FB0B30")
                elif r == "Low Flow":
                    row_color.append("#FB0B30")
                elif r == "High Flow":
                    row_color.append("#FB0B30")
                elif r == "Hi Pressure Repeat Test":
                    row_color.append("#F15317")
                elif r == "Low Pressure Repeat Test":
                    row_color.append("#F15317")
                elif r == "Fail, Gross Leak":
                    row_color.append("#FB0B30")
                elif r == "Fail, Leak":
                    row_color.append("#FB0B30")
                else:
                    row_color.append("#F1E117")
            res_colors.append(row_color)
        table_fig, table_ax = plt.subplots(figsize=(15, 8))

        # table_fig.patch.set_visable(False)
        table_ax.axis('off')
        table_ax.axis('tight')
        res_table = table_ax.table(cellText=part_result_df.values, cellColours=res_colors, colLabels=columns,
                                   colWidths=[0.2, 0.2, 0.3], rowLabels=new_rows, rowLoc='center', loc='center')
        res_table.set_fontsize(16)
        res_table.scale(1, 3)
        table_fig.tight_layout()
        plt.suptitle(self.name)
        plt.subplots_adjust(left=0.1, right=1.05)
        plt.savefig(self.name+"_ResultsTable.jpg")

    def classify_lot(self):
        #classifies lot type
        dirs = all_dir()
        for d in dirs:
            if d[-3:] == "st5":
                self.sub_batch.append(d)   #add to subbatches
            elif len(d.split("_")[-1]) < 2:
                self.sub_batch.append(d)   #add to subbatches
            elif d == 'ResultPlots':
                continue
            else:
                self.add_part(d)  #if lot contains dirs of parts, adds parts
        #if lot contains sub_batches, process those as individual Lots, combine for overall Lot
        if len(self.sub_batch)>0:
            self.process_subs()
            self.make_resultfile(True)  # output results and plots
        else:#possibly return all cumulative batch data
            self.make_resultfile(False)#output results and plots   #no, access and append durring process_subs

    def process_subs(self):
        #loop through subs
        print("Found %s Sub Batches"%str(len(self.sub_batch)))
        for sub in self.sub_batch:
            print("Processing %s"%sub)
            os.chdir(sub)
            sub_lot = Lot()  # Analyze sub lot
            #pass sub_lot data up to parent Lot
            if self.flow_df.empty:
                self.flow_df = sub_lot.flow_df
            else:
                self.flow_df = pd.concat([self.flow_df, sub_lot.flow_df])
            if self.tor_stat_df.empty:
                self.tor_stat_df = sub_lot.tor_stat_df
            else:
                self.tor_stat_df = pd.concat([self.tor_stat_df, sub_lot.tor_stat_df])
            self.pd_rowdict_df = pd.concat([self.pd_rowdict_df, sub_lot.pd_rowdict_df])
            self.flow_tor_plot_df = pd.concat([self.flow_tor_plot_df, sub_lot.flow_tor_plot_df], axis=1)
            self.pd_tor_plot_df = pd.concat([self.pd_tor_plot_df, sub_lot.pd_tor_plot_df], axis=1)
            # pass sub_lot parts to parent part list
            for part in sub_lot.parts:
                self.parts.append(part)
            #check sub_lot dates
            if self.start_date == None:
                self.start_date = sub_lot.start_date
            elif int(sub_lot.start_date) < int(self.start_date):
                self.start_date = sub_lot.start_date
            if self.end_date == None:
                self.end_date = sub_lot.end_date
            elif int(sub_lot.end_date) > int(self.end_date):
                self.end_date = sub_lot.end_date
            os.chdir(self.lot_dir)
        print("Done Processing Sub Batches.")
        print("Combining Sub Batches.")

#Part is mostly done now.  Unsure if I need to track anything else
class Part:
    #on init use classify_sheets() to scrape
    def __init__(self):
        self.name = find_part_name()
        self.lot = self.name[:6]
        flow, flow_torque, port0_pd, port5_pd, port11_pd, port16_pd, pd_torque = classify_sheets()
        self.flow = flow
        self.flow_torque = flow_torque
        self.port0_pd = port0_pd
        self.port5_pd = port5_pd
        self.port11_pd = port11_pd
        self.port16_pd = port16_pd
        self.pd_torque = pd_torque
        self.summary = self.make_summary()
        try:
            self.date = flow.split("_")[1][:8]
        except AttributeError:
            self.date = None

    def make_summary(self):
        """Combines .csv raw data into single file .xslx 'Summary'
        Sets part.summary equal to fileName"""
        #local vars
        markers = ['triangle', 'circle', 'square', 'diamond']
        colors = ['blue', 'green', 'cyan', 'orange', 'purple', 'magenta', 'red', 'brown', 'yellow', 'silver', 'lime']
        #pandas output
        summary_filename = self.name + "_Summary.xlsx"
        writer = pd.ExcelWriter(summary_filename, engine='xlsxwriter')
        workbook = writer.book
        excel_wss = all_files(".xlsx")  #check for excel sheets
        if len(excel_wss) == 0:  #if no summary
            #start scraping rawdata
            ##Flow
            if self.flow != None:
                flow_df = parse_flow(self.flow)
                rot_flow_df = flow_df.astype(
                    {'Segment': str, 'Time': np.float64, 'Pressure': np.float64, 'Flow': np.float64})
                rot_flow_df.to_excel(writer, sheet_name="Rotary Flow Test")
                rot_flow_sh = writer.sheets['Rotary Flow Test']
                rot_flow_chart = workbook.add_chart({'type': 'scatter', 'subtype': 'smooth_with_markers'})
                rot_flow_chart.add_series({
                    'name': 'Rotary Flow Test',
                    'categories': ["Rotary Flow Test", 1, 2, (len(rot_flow_df['Time'].values) + 1), 2],
                    'values': ["Rotary Flow Test", 1, 4, (len(rot_flow_df['Flow'].values) + 1), 4],
                    'marker': {
                        'type': markers[1],
                        'border': {'color': 'black'},
                        'fill': {'color': colors[0]},
                        'size': 8
                    },
                    'line': {'color': colors[0]}
                })
                rot_flow_chart.set_x_axis({
                    'name': 'Time (s)',
                    'name_font': {'size': 16, 'bold': True}
                })
                rot_flow_chart.set_y_axis({
                    'name': 'Flow (sccm)',
                    'name_font': {'size': 16, 'bold': True}
                })
                rot_flow_chart.set_title({
                    'name': 'Flow Test During Rotary Movement',
                    'name_font': {'size': 18, 'bold': True}
                })
                rot_flow_sh.insert_chart('G2', rot_flow_chart)
            else:
                empty_df = pd.DataFrame(columns=['Time', 'Flow'])
                empty_df.to_excel(writer, sheet_name="Rotary Flow Test")
            ##Flow Torque
            if self.flow_torque != None:
                rot_flow_tdf = parse_torque(self.flow_torque)
                rot_flow_tdf.to_excel(writer, sheet_name="Rotary Flow Torque")
                rot_flowT_sh = writer.sheets['Rotary Flow Torque']
                rot_flowT_chart = workbook.add_chart({'type': 'scatter', 'subtype': 'smooth_with_markers'})
                rot_flowT_chart.add_series({
                    'name': 'Rotary Flow Torque',
                    'categories': ["Rotary Flow Torque", 1, 1, (len(rot_flow_tdf.iloc[:, 0].values) + 1), 1],
                    'values': ["Rotary Flow Torque", 1, 2, (len(rot_flow_tdf.iloc[:, 1].values) + 1), 2],
                    'marker': {
                        'type': markers[1],
                        'border': {'color': 'black'},
                        'fill': {'color': colors[0]},
                        'size': 8
                    },
                    'line': {'color': colors[0]}
                })
                rot_flowT_chart.set_x_axis({
                    'name': 'Time (s)',
                    'name_font': {'size': 16, 'bold': True}
                })
                rot_flowT_chart.set_y_axis({
                    'name': 'Torque (N-m)',
                    'name_font': {'size': 16, 'bold': True}
                })
                rot_flowT_chart.set_title({
                    'name': 'Rotary Torque During Flow Test',
                    'name_font': {'size': 18, 'bold': True}
                })
                rot_flowT_sh.insert_chart('E2', rot_flowT_chart)
            else:
                empty_df = pd.DataFrame(columns=['No Flow Data'])
                empty_df.to_excel(writer, sheet_name="Rotary Flow Torque")
            ##PD Ports
            portpd_arr = []   #list containing each port PD resultDF
            keyss = []        #list of strings 'Port XXX' if results are avail for that port
            if self.port0_pd != None:
                port0pd_df = parse_portPD(self.port0_pd)
                port0pd_df = port0pd_df.reset_index()
                port0pd_df = port0pd_df.loc[:, ~port0pd_df.columns.str.contains('^index')]
                port0pd_df = port0pd_df.loc[:, ~port0pd_df.columns.str.contains('^Time unit')]
                port0pd_df = port0pd_df.astype({'Segment': str, 'Time': np.float64, 'Pressure': np.float64})
                port0pd_df = port0pd_df.rename(columns={'Segment': 'Segment 0', 'Time': 'Time 0', 'Pressure': 'Pressure 0'})
                portpd_arr.append(port0pd_df)
                keyss.append('Port 0')
            if self.port5_pd != None:
                port5pd_df = parse_portPD(self.port5_pd)
                port5pd_df = port5pd_df.reset_index()
                port5pd_df = port5pd_df.loc[:, ~port5pd_df.columns.str.contains('^index')]
                port5pd_df = port5pd_df.loc[:, ~port5pd_df.columns.str.contains('^Time unit')]
                port5pd_df = port5pd_df.astype({'Segment': str, 'Time': np.float64, 'Pressure': np.float64})
                port5pd_df = port5pd_df.rename(
                    columns={'Segment': 'Segment 5.5', 'Time': 'Time 5.5', 'Pressure': 'Pressure 5.5'})
                portpd_arr.append(port5pd_df)
                keyss.append('Port 5.5')
            if self.port11_pd != None:
                port11pd_df = parse_portPD(self.port11_pd)
                port11pd_df = port11pd_df.reset_index()
                port11pd_df = port11pd_df.loc[:, ~port11pd_df.columns.str.contains('^index')]
                port11pd_df = port11pd_df.loc[:, ~port11pd_df.columns.str.contains('^Time unit')]
                port11pd_df = port11pd_df.astype({'Segment': str, 'Time': np.float64, 'Pressure': np.float64})
                port11pd_df = port11pd_df.rename(
                    columns={'Segment': 'Segment 11.5', 'Time': 'Time 11.5', 'Pressure': 'Pressure 11.5'})
                portpd_arr.append(port11pd_df)
                keyss.append('Port 11.5')
            if self.port16_pd != None:
                port16pd_df = parse_portPD(self.port16_pd)
                port16pd_df = port16pd_df.reset_index()
                port16pd_df = port16pd_df.loc[:, ~port16pd_df.columns.str.contains('^index')]
                port16pd_df = port16pd_df.loc[:, ~port16pd_df.columns.str.contains('^Time unit')]
                port16pd_df = port16pd_df.astype({'Segment': str, 'Time': np.float64, 'Pressure': np.float64})
                port16pd_df = port16pd_df.rename(
                    columns={'Segment': 'Segment 16.5', 'Time': 'Time 16.5', 'Pressure': 'Pressure 16.5'})
                portpd_arr.append(port16pd_df)
                keyss.append('Port 16.5')
            #Handling Missing Data
            ##no port pd results
            if len(portpd_arr) == 0:
                portpd_df = pd.DataFrame(columns=["MISSING PORT PD DATA"])
                portpd_df.to_excel(writer, sheet_name="Port PD Tests")
            ## one PD result
            elif len(portpd_arr) == 1:
                portpd_df = portpd_arr[0]
                portpd_df.to_excel(writer, sheet_name="Port PD Tests")
            ## concat PD results in list = portpd_arr
            else:
                portpd_df = pd.concat(portpd_arr, axis=1)
                portpd_df.to_excel(writer, sheet_name="Port PD Tests")
            #PD torque
            if self.pd_torque != None:
                port_pdt_df = parse_torque(self.pd_torque)
                port_pdt_df.to_excel(writer, sheet_name="Port PD Torque")
            else:
                port_pdt_df = pd.DataFrame(columns=["Time (sec)", 'Torque (N-m)'])

            #start plotting PD and PDTor
            ## Port PD
            try:
                portpd_sh = writer.sheets['Port PD Tests']
                pd_port_chart = workbook.add_chart({'type': 'scatter', 'subtype': 'smooth_with_markers'})
                ports = ['0', '5.5', '11.5', '16.5']
                chart = False
                for i in range(int(len(portpd_df.columns) / 3)):
                    zero_cols = portpd_df.columns[portpd_df.columns.str.contains(pat=ports[i])]
                    zero_df = portpd_df[zero_cols]
                    pressure_col = zero_df.iloc[:, 2].values
                    if len(pressure_col) < 20:
                        val = pressure_col[0]
                        if val < 4.0:
                            # print("Low P:  %s"%(str(val)))
                            pass
                        elif val > 5.5:
                            # print("High P:  %s"%(str(val)))
                            pass
                        else:
                            # print("interupted test?")
                            pass
                    else:
                        chart = True
                        pd_port_chart.add_series({
                            'name': 'Port %s PD Test' % ports[i],
                            'categories': ["Port PD Tests", 1, (14), (len(pressure_col) - 20), (14)],
                            'values': ["Port PD Tests", 21, (3 + i * 3), (len(pressure_col) + 1), (3 + i * 3)],
                            'marker': {
                                'type': markers[i],
                                'border': {'color': 'black'},
                                'fill': {'color': colors[i]},
                                'size': 8
                            },
                            'line': {'color': colors[i]}
                        })
                pd_port_chart.set_x_axis({
                    'name': 'Time (s)',
                    'name_font': {'size': 16, 'bold': True}
                })
                pd_port_chart.set_y_axis({
                    'name': 'Pressure (psi)',
                    'name_font': {'size': 16, 'bold': True}
                })
                pd_port_chart.set_title({
                    'name': 'SRV Leak Tests',
                    'name_font': {'size': 18, 'bold': True}
                })
                if chart:
                    insert_dict = {'Port PD Plot Index': np.arange(0, 5, 0.1)}
                    insert_df = pd.DataFrame.from_dict(insert_dict)
                    insert_df.to_excel(writer, sheet_name='Port PD Tests', startcol=13)
                    portpd_sh.insert_chart('P2', pd_port_chart)
                #only plots PD if complete test
                port_pdt_sh = writer.sheets['Port PD Torque']
                #PD torque plot
                pd_tor_chart = workbook.add_chart({'type': 'scatter', 'subtype': 'smooth_with_markers'})
                pd_tor_chart.add_series({
                    'name': 'Leak Test Rotary Torque',
                    'categories': ["Port PD Torque", 1, 1, (len(port_pdt_df["Time (sec)"].values) + 1), 1],
                    'values': ["Port PD Torque", 1, 2, (len(port_pdt_df['Torque (N-m)'].values) + 1), 2],
                    'marker': {
                        'type': markers[1],
                        'border': {'color': 'black'},
                        'fill': {'color': colors[0]},
                        'size': 8
                    },
                    'line': {'color': colors[0]}
                })
                pd_tor_chart.set_x_axis({
                    'name': 'Time (s)',
                    'name_font': {'size': 16, 'bold': True}
                })
                pd_tor_chart.set_y_axis({
                    'name': 'Torque (N-m)',
                    'name_font': {'size': 16, 'bold': True}
                })
                pd_tor_chart.set_title({
                    'name': 'Rotary Torque for Leak Tests',
                    'name_font': {'size': 18, 'bold': True}
                })
                port_pdt_sh.insert_chart('E2', pd_tor_chart)
                writer.save()
            except KeyError:
                return 0
            except AttributeError:
                return 0
        elif len(excel_wss) == 1:  #if one summary
            if excel_wss[0] == summary_filename:   #if summary is current
                return summary_filename
            else:
                os.remove(excel_wss[0])   #if old summary
                return self.make_summary()
        else:    #if multiple summaries found
            for f in excel_wss:
                os.remove(f)
            return self.make_summary()
        return summary_filename


if __name__ == "__main__":
    print("SRV OQC Auto Data Analysis Tool\n")
    first_pass_analysis(False)
    print("\n")
    response = "I"
    while response != "E":
        response = input("A\t|\tAnalyze Specific Batch\nE\t|\tExit\n\n")
        if response == "A":
            print("Specific Batch")
            r = analyze_specific_batch()
            if r == 0:
                print("Continue....")
            else:
                response = "E"
        elif response == "M":
            first_pass_analysis(True)
        elif response == "E":
            print("Exiting...")
            continue
        else:
            print("Invalid Response")
