import threading
import queue
import socket
import time
import sys
import os
import logging
import kivy.resources
from datetime import date
import numpy as np
import pandas as pd
import serial as ser
from multiprocessing.pool import ThreadPool
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen, ScreenManager
import serial.tools.list_ports
from kivy.properties import ListProperty


class testArea:

    def __init__(self, ard=None, mk_ten=None, isaac=None, pressDistance=None, testSpecs=None):
        self._ard = ard
        self._mk_ten = mk_ten
        self._isaac = isaac
        self._testSpecs = testSpecs
        self.lock = threading.Lock()
        self.pressDistance = 0.45
        self.partID = '000'


    @property
    def ard(self):
        return self._ard
    @ard.setter
    def ard(self, ardComSel):
        if ardComSel == None:
            self._ard = None
        elif ardComSel[:3] != 'COM':
            print("str process error")
            self._ard = None
        else:
            try:
                print("setting ard")
                self._ard = self.initSer(ardComSel)
            except ser.serialutil.SerialException:
                self._ard = None
            if self.ard == None:
                logging.info("Ard Could'nt Init Serial")
            else:
                try:
                    self._ard.write(b'I')
                    self.resetALRM()
                except TimeoutError:
                    self._ard = None

    @property
    def mk_ten(self):
        return self._mk_ten
    @mk_ten.setter
    def mk_ten(self, mktenCOMsel):
        if mktenCOMsel == None:
            self._mk_ten = None
        elif mktenCOMsel[:3] != 'COM':
            self._mk_ten = None
        else:
            try:
                self._mk_ten = self.initSer(mktenCOMsel)
            except ser.serialutil.SerialException:
                self._mk_ten = None
            if self.mk_ten == None:
                logging.info("Mkten Could'nt Init Serial")
            else:
                try:
                    self.readMk10()
                except TimeoutError:
                    logging.info("Read Mk10 Timeout")
                    self._mk_ten = None

    @property
    def isaac(self):
        return self._isaac
    @isaac.setter
    def isaac(self, isaacIPinput):
        if isaacIPinput == None:
            self._isaac = None
        elif len(isaacIPinput.split('.'))!=4:
            self._isaac = None
        else:
            try:
                self._isaac = self.initPressure(isaacIPinput)
            except TimeoutError:
                self._isaac = None
            if self.isaac == None:
                pass
            else:
                try:
                    type = self.isaac.send('RTT]'.encode())
                    ret = self.isaac.recv(5)
                except TimeoutError:
                    self._isaac = None

    @property
    def testSpecs(self):
        return self._testSpecs
    @testSpecs.setter
    def testSpecs(self, testSpecsInput):
        self.testSpecs = testSpecsInput

    def initPressure(self, ip='192.168.1.2', port=23, buffSize=96):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ip, port))
        s.settimeout(15)
        return s
    def initSer(self, inputPort):
        """Inits serial connection to kwarg string port"""
        cer = ser.Serial()
        cer.baudrate = 115200
        cer.port = inputPort
        cer.timeout = 10
        cer.open()
        return cer

    def check_rdy(self):
        """Checks READY and PLS-RDY output signals and waits until ready, recursive
        IN - cer - pyserial.Serial Object
        OUT -> rdy - (0 if error), (1 if both ready)"""
        rdy = 0
        if self.ard.is_open:
            while self.check_move():
                time.sleep(0.5)
            self.ard.write(b'>')
            r = self.ard.read(4)
            if r == b'rrdy':
                self.ard.write(b'&')
                pr = self.ard.read(4)
                if pr == b'plsr':
                    rdy = 1
                elif pr == b'plnr':
                    rdy = self.check_rdy()
                else:
                    print("IO Error 2")
                    logging.info("Serial IO error, PULSE-RDY")
            elif r == b'nrdy':
                rdy = self.check_rdy()
            else:
                print("IO Error 1")
                logging.info("Serial IO error, READY")
        else:
            print("Serial Error, Not Open")
            logging.info("Check Ready Serial COM Error")
        return rdy
    def resetALRM(self):
        """Resets OrientalMotor Driver Alarm, 100ms delay
        IN - cer - pyserial Serial Object
        OUT - (0 if successful), (1 if error)"""
        self.ard.write(b'%')
        r = self.ard.read(4)
        if (r == b'arst'):
            return 0
        else:
            return 1
    def check_move(self):
        """Checks if MOVE signal and moving.
        OUT - (0 if not moving), (1 if moving), (2 if error)"""
        if self.ard.is_open:
            self.ard.write(b'*')
            r = self.ard.read(4)
            if r == b'mmmm':
                return 1
            elif r == b'fmmm':
                return 0
            else:
                print("move error")
                logging.info("Serial COM error - check_move - MOVE")
                return 2
        else:
            return 2
    def jog_fwd(self, pulses):
        """Conducts FWD JOG operation.  Not exact number of pulses.  Speed/pulse_per_distance not set yet MEXE02
        IN : cer - serial object (arudino),
             pulses - Number of Pulses to JOG FWD
        OUT -. (0 if successful), (1 if unsuccessful)"""
        if (pulses == 0):
            return 0
        travel_amt = 0.01  # mm
        op_spd = 6.64  # mm/s
        a = 10000  # m/s^2 accel and decel
        start_spd = 5  # mm/s
        op_spd_h = 50  # high speed maneuvering (how?) lol
        # dead distance (due to acceleration is 1 pulse)
        ret = 1
        trav = pulses * travel_amt - travel_amt  # mm to travel
        jog_pulse_time = trav / op_spd  # in second
        self.check_rdy()
        self.ard.write(b'$')
        if self.ard.read(4) == b'fwjg':
            time.sleep(jog_pulse_time)
            self.ard.write(b'$')
            if self.ard.read(4) == b'stfj':
                ret = 0
            else:
                self.ard.write(b'!')  # STOP toggle
                ret = 1
        else:
            print("Serial Com Error Jog Fwd")
            logging.info("Serial COM error - jogFwd")
        return ret
    def toggleFWDjog(self):
        """Toggles FWD Jog.
        IN:  cer - pyserial Serial object (arduino)
        OUT: (0 if stop jog), (1 if jog), (2 if error, toggles Stop)"""
        self.ard.write(b'$')
        r = self.ard.read(4)
        if (r == b'fwjg'):
            return 1
        elif (r == b'stfj'):
            return 0
        else:
            stop = self.toggleSTOP()
            if stop == 0:
                stop = self.toggleSTOP()
            return 2
    def jog_rev(self, pulses):
        """Conducts REVERSE JOG operation.  Not exact number of pulses.  Speed/pulse_per_distance not set yet MEXE02
            IN : cer - serial object (arudino),
                 pulses - Number of Pulses to JOG FWD
            OUT -. (0 if successful), (1 if unsuccessful)"""
        if (pulses == 0):
            return 0

        travel_amt = 0.01  # mm
        op_spd = 6.64  # mm/s
        ret = 1
        trav = pulses * travel_amt - travel_amt  # mm to travel
        jog_pulse_time = trav / op_spd  # in second
        self.check_rdy()

        if self.ard.is_open:
            self.ard.write(b'#')
            if self.ard.read(4) == b'rvjg':
                time.sleep(jog_pulse_time)
                self.ard.write(b'#')
                if self.ard.read(4) == b'strj':
                    ret = 0
                else:
                    stop = self.toggleSTOP()  # STOP toggle
                    ret = stop
            else:
                print("Serial Com Error Jog Rev")
        else:
            print("Serial Not Open Rev Jog")
            logging.info("Serial COM error revJog")
        return ret
    def toggleREVjog(self):
        """Toggles REV Jog.
        IN:  cer - pyserial Serial object (arduino)
        OUT: (0 if stop jog), (1 if jog), (2 if error, toggles Stop)"""
        self.ard.write(b'#')
        r = self.ard.read(4)
        if (r == b'rvjg'):
            return 1
        elif (r == b'strj'):
            return 0
        else:
            stop = self.toggleSTOP()
            if stop == 0:
                stop = self.toggleSTOP()
            return 2
    def zHOME(self):
        """Conducts ZHOME operation.  Speed not set/unsure, set with MEXE02
        IN - cer - serial object (arduino)
        OUT - (0 if successful), (1 if unsuccessful)"""
        # check_ready
        if self.check_rdy() == 1:
            self.ard.write(b' ')
            r = self.ard.read(4)
            if r == b'zzzz':
                return 0
            else:
                return 1
        else:
            return 1
    def end_HOME(self):
        """Checks for HOME-END output signal
        IN - cer - serial object (arduino)
        OUT - (0 if home), (1 if unsuccessful) """
        self.ard.write(b'*')
        r = self.ard.read(4)
        if r == b'mmmm':
            time.sleep(1)
            return self.end_HOME()
        elif r == b'fmmm':
            self.ard.write(b'@')
            r = self.ard.read(4)
            while (r == b'wait'):
                self.ard.write(b'@')
                r = self.ard.read(4)
            if r == b'HOME':
                return 0
            else:
                return 1
        else:
            return 1
    def toggleFree(self):
        """Frees the Motor and magnetic brake, allowing manual movement of the slide.  WARNING this will cause the slide to drop unless held.
        IN - cer - serial object (arduino)
        OUT - (O if free), (1 if locked), (2 if unsuccessful)"""
        self.ard.write(b'=')
        r = self.ard.read(4)
        if (r == b'free'):
            logging.info("Free Successful Return - Free")
            return 0
        elif (r == b'ntfr'):
            logging.info("Free Successful Return - Not Free")
            return 1
        else:
            logging.info(("Serial Com. Error - Free"))
            return 2
    def toggleSTOP(self):
        """Emergency STOP signal for motor and actuator.  Toggle.
        IN - cer - serial object (arduio)
        OUT - (0 if return), (1 if STOP), (2 if unsuccessful)"""
        self.ard.write(b'!')
        r = self.ard.read(4)
        if (r == b'stop'):
            return 1
        elif (r == b'ress'):
            return 0
        else:
            return 2
    def checkALRM(self):
        """Checks alarm signal from OriMotor Driver
        IN - cer - pyserial Serial Object (arduino)
        OUT - (0 if no ALRM), (1 if ALRM), (2 if error)"""
        self.ard.write(b'A')
        r = self.ard.read(4)
        if (r == b'alrm'):
            return 1
        elif (r == b'cont'):
            return 0
        else:
            return 2
    def pulseOPcw(self, dist, speed):
        """Conducts clockwise pulse operation, based on distance and speed.
        IN:  cer - pyserial Serial Object (arduino)
             dist - Distance in (mm) to move slide (clockwise is DOWN)
             speed - Speed in mm/s to move
        OUT:    (0 if complete), (1 if error)"""

        if (dist == 0):
            return 0
        pulses = int(dist / 0.01)  # number of pulses to move
        pulse_per_second = speed / 0.01  # number of pulses per second to meet speed req
        cycle_time = 1.0 / pulse_per_second  # (float) seconds time for each duty cycle
        duty_time = cycle_time / 2.0  # (float) seconds time for each half of duty cycle
        maxPulseCycleTime = (115200 / 2.0) * (0.01)  # max pulse/sec

        if pulse_per_second > maxPulseCycleTime:
            print("Max Pulse Cycle Time, %s" % maxPulseCycleTime)
            return 1
        else:
            for i in range(pulses):
                self.ard.write(b'+')
                time.sleep(duty_time)
                self.ard.write(b'+')
                time.sleep(duty_time)
            return 0
    def pulseOPccw(self, dist, speed):
        """Conducts counter-clockwise pulse operation, based on distance and speed.
            IN:  cer - pyserial Serial Object (arduino)
                 dist - Distance in (mm) to move slide (counter=clockwise is UP)
                 speed - Speed in mm/s to move
            OUT:    (0 if complete), (1 if error)"""
        if (dist == 0):
            return 0
        pulses = int(dist / 0.01)  # number of pulses to move
        pulse_per_second = speed / 0.01  # number of pulses per second to meet speed req
        cycle_time = 1.0 / pulse_per_second  # (float) seconds time for each duty cycle
        duty_time = cycle_time / 2.0  # (float) seconds time for each half of duty cycle
        maxPulseCycleTime = (115200 / 2.0) * (0.01)  # max pulse/sec

        if pulse_per_second > maxPulseCycleTime:
            return 1
        else:
            for i in range(pulses):
                self.ard.write(b'-')
                time.sleep(duty_time)
                self.ard.write(b'-')
                time.sleep(duty_time)
            return 0
    def pulseOPcwFast(self, dist):
        """Conducts counter-clockwise pulse operation, based on distance and speed.
            IN:  cer - pyserial Serial Object (arduino)
                 dist - Distance in (mm) to move slide (clockwise is DOWN)
                 speed - 4.81mm/s
            OUT:    (0 if complete), (1 if error)"""
        if (dist == 0):
            return 0
        pulses = int(dist / 0.01)  # number of pulses to move
        if pulses % 2 == 0:
            pulses = int(pulses / 2)
            pass
        else:
            pulses -= 1
            pulses=int(pulses / 2)
            self.ard.write(b'm')
        for i in range(pulses):
            self.ard.write(b'o')
            time.sleep(0.0025)
        return 0
    def pulseOPccwFast(self, dist):
        """Conducts counter-clockwise pulse operation, based on distance and speed.
            IN:  cer - pyserial Serial Object (arduino)
                 dist - Distance in (mm) to move slide (counter=clockwise is UP)
                 speed - Speed in mm/s to move
            OUT:    (0 if complete), (1 if error)"""
        if (dist == 0):
            return 0
        pulses = int(dist / 0.01)  # number of pulses to move
        if pulses % 2 == 0:
            pulses = int(pulses / 2)
            pass
        else:
            pulses -= 1
            pulses=int(pulses / 2)
            self.ard.write(b'n')
        for i in range(pulses):
            self.ard.write(b'p')
            time.sleep(0.0025)
        return 0
    def pulseOPcwFastEx(self, dist):
        """Conducts counter-clockwise pulse operation, based on distance and speed.
            IN:  cer - pyserial Serial Object (arduino)
                 dist - Distance in (mm) to move slide (clockwise is DOWN)
                 speed - 4.81mm/s
            OUT:    (0 if complete), (1 if error)"""
        if (dist == 0):
            return 0
        pulses = int(dist / 0.01)  # number of pulses to move
        for i in range(pulses):
            self.ard.write(b'm')
            time.sleep(0.00415)
        return 0
    def pulseOPccwFastEx(self, dist):
        """Conducts counter-clockwise pulse operation, based on distance and speed.
            IN:  cer - pyserial Serial Object (arduino)
                 dist - Distance in (mm) to move slide (counter=clockwise is UP)
                 speed - Speed in mm/s to move
            OUT:    (0 if complete), (1 if error)"""
        if (dist == 0):
            return 0
        pulses = int(dist / 0.01)  # number of pulses to move
        for i in range(pulses):
            self.ard.write(b'n')
            time.sleep(0.00415)
        return 0

    def readMk10(self):
        """"Reads current force reading on Mk10 Force gauge.
        IN:  cerMk10 - Mark10force gauge Serial Object
        OUT:  float curForce - current force  Newton"""
        qq = str.encode('?\r\n')
        self.mk_ten.write(qq)
        try:
            rdln = self.mk_ten.readline()
        except TimeoutError:
            logging.info("Mkten Timeout Error")
            return None
        if rdln == b'':
            return None
        elif rdln == b'ninp\r\n':
            logging.info("Mk10 Connection Pointed at Arduino")
            return None
        elif rdln == b'inpo\r\n':
            logging.info("Mk10 Connection Pointed at Arduino")
            return None
        split = rdln.split(b' ')
        try:
            curForce = float(split[0])
        except:
            curForce = 0.0
        return curForce
    def findPlunger(self):
        test = self.readMk10()
        rd = self.check_rdy()
        pulse = self.pulseOPccwFast(2)
        ret = self.check_rdy()
        jog = self.toggleFWDjog()
        while (test <= 0.5):
            if jog == 1:
                test = self.readMk10()
            else:
                jog = self.toggleFWDjog()
        jog = self.toggleFWDjog()
        rdy = self.check_rdy()
        pulse = self.pulseOPccwFast(1.5)
        test = self.readMk10()
        while (test <= 0.5):
            self.pulseOPcw(0.01, 0.2)
            test = self.readMk10()
        logging.info("Found Plunger")
        return 0

    def isaacChangeProgram(self, prog):
        """Uses Telnet commands to change IsaacHD program
        IN: sockIsaacHD - IsaacHD Socket Object
            prog - 2 digit unicode string of desired program.  Ex = '01'
        OUT: (0 if successful), (1 if error)"""
        change = 'SCP'
        changeProg = change + prog + ']'
        self.isaacFlushSocket()
        self.isaac.send(changeProg.encode())
        time.sleep(1)
        self.isaac.send('RCP]'.encode())
        ret = self.isaac.recv(5)
        cur_prog = repr(ret).split("\\")[0].split("'")[1]
        if cur_prog == prog:
            return 0
        else:
            return 1
    def isaacFlushSocket(self):
        """Flushes any pending data in socket buffer.  Prepares for test output.
        IN: sockIsaacHD - IsaacHD Socket Object
        OUT: None"""
        self.isaac.settimeout(3)
        r=False
        while(r==False):
            try:
                flush = self.isaac.recv(1024)
            except socket.timeout:
                r = True
        self.isaac.settimeout(10)

    def move_start_to_unpress(self):
        #moves motor at start from zhome to unpress position
        self.findPlunger()
        return 0
    def move_pinchPVA(self):

        d = float(self.pressDistance)
        logging.info("Moving to Pinch, %s" %str(d))
        data = np.zeros(30)
        dist = np.zeros(30)
        i=0
        data[i] = self.readMk10()
        dist[i] = 0.0
        print(dist, data)
        move=0.05
        while move == 0.05:
            i+=1
            r = self.pulseOPcwFast(0.05)
            time.sleep(0.05/4.81)
            data[i] = self.readMk10()
            dist[i] = move*i
            print(dist[:i], data[:i])
            d -= move
            if d >= 0.05:
                move = 0.05
            else:
                move = 0
        d = round(d, 2)
        if d>0:
            print(d)
            i+=1
            r = self.pulseOPcwFast(d)
            time.sleep(d/4.81)
            data[i] = self.readMk10()
            dist[i] = 0.05*(i-1) + d
        i+=1
        dist = dist[:i]
        data = data[:i]
        print(dist, data)
        logging.info("Pinch Complete")
        out_dict = {'Distance Move Pinch (mm)':dist, 'Pinch Force (N)':data}
        return out_dict

    def move_unpinchPVA(self):

        d = float(self.pressDistance)
        logging.info("Moving to Pinch, %s" % str(d))
        data = np.zeros(30)
        dist = np.zeros(30)
        i = 0
        data[i] = self.readMk10()
        dist[i] = 0.0
        print(dist, data)
        move = 0.05
        while move == 0.05:
            i += 1
            r = self.pulseOPccwFast(0.05)
            data[i] = self.readMk10()
            dist[i] = move * i
            print(dist[:i], data[:i])
            d -= move
            if d >= 0.05:
                move = 0.05
            else:
                move = 0
        d = round(d, 2)
        if d > 0:
            print(d)
            i += 1
            r = self.pulseOPccwFast(d)
            data[i] = self.readMk10()
            dist[i] = 0.05 * (i - 1) + d
        i+=1
        dist = dist[:i]
        data = data[:i]
        print(dist, data)
        logging.info("Release Pinch Complete")
        out_dict = {'Distance Move Release (mm)': dist, 'Unpinch Force (N)': data}
        return out_dict

    def isaacSet_flow_test_short(self, flow_p, flow_t):
        lowSpecStr = 'SFL' + str(1.0) + ']'
        highSpecStr = 'SFH' + str(315.0) + ']'
        flow_press = 'STP'+str(flow_p) + ']'
        specTime = 'ST6' + str(flow_t) + ']'
        ret = self.isaacChangeProgram('07')
        time.sleep(0.5)
        if ret == 0:
            time.sleep(0.5)
            settings = []
            settings.append('SSL3]'.encode())
            settings.append('STT4]'.encode())
            settings.append(flow_press.encode())
            settings.append('SPTP0.50]'.encode())
            settings.append('SPTM0.50]'.encode())
            settings.append('ST30.0]'.encode())
            settings.append('ST42.0]'.encode())
            settings.append('ST52.0]'.encode())
            settings.append(specTime.encode())
            settings.append('ST71.0]'.encode())
            settings.append('SVA1]'.encode())
            settings.append('SFA0]'.encode())
            settings.append(lowSpecStr.encode())
            settings.append(highSpecStr.encode())
            settings.append('SLD3]'.encode())
            settings.append('SER3]'.encode())
            for setting in settings:
                self.isaac.send(setting)
                time.sleep(0.1)
            return 1
        else:
            return 0
    def isaacSet_pva_seal(self, seal_p, seal_t, seal_drop):
        lowSpecStr = 'SML' + str(seal_drop) + ']'
        highSpecStr = 'SMD' + str(seal_drop) + ']'
        specTime = 'ST6' + str(seal_t) + ']'
        test_pressure = 'STP' +str(seal_p) + ']'
        ret = self.isaacChangeProgram('06')
        if ret == 0:
            time.sleep(0.5)
            settings = []
            settings.append('SSL3]'.encode())
            settings.append('STT0]'.encode())
            settings.append(test_pressure.encode())
            settings.append('SPTP0.50]'.encode())
            settings.append('SPTM0.50]'.encode())
            settings.append('ST30.0]'.encode())
            settings.append('ST42.0]'.encode())
            settings.append('ST52.0]'.encode())
            settings.append(specTime.encode())
            settings.append('ST71.0]'.encode())
            settings.append('SVA1]'.encode())
            settings.append('SFA0]'.encode())
            settings.append(lowSpecStr.encode())
            settings.append(highSpecStr.encode())
            settings.append('SLD1]'.encode())
            settings.append('SER2]'.encode())
            for setting in settings:
                self.isaac.send(setting)
                time.sleep(0.1)
            return 1
        else:
            return 0

    def isaacRun_flow_short(self, move):
        logging.info("Isaac Flow Test %s, start"%str(move))
        self.isaacChangeProgram('07')
        self.isaacFlushSocket()
        test_time_len = 62
        test_samples = test_time_len * 10
        segments = []
        times = []
        pressures = []
        flows = []
        self.isaac.send('SRP]'.encode())
        for i in range(test_samples):
            try:
                rawOutputStr = self.isaac.recv(100).split(b'\t')
                rawoutput1 = self.isaac.recv(100).split(b'\t')
                print(rawOutputStr)
                print(rawoutput1)
            except socket.timeout:
                rawOutputStr = None
                rawoutput1 = None
            if i<20:
                pass
            else:
                #segment, time, pressure, flow
                if len(rawOutputStr) > 4:
                    segments.append(rawOutputStr[0])
                    times.append(rawOutputStr[1])
                    pressures.append(rawOutputStr[3])
                    flows.append(rawOutputStr[4])
                elif len(rawoutput1) > 4:
                    segments.append(rawoutput1[0])
                    times.append(rawoutput1[1])
                    pressures.append(rawoutput1[3])
                    flows.append(rawoutput1[4])
                else:
                    segments.append('Err')
                    times.append('err')
                    pressures.append('err')
                    flows.append('err')
            if i == 30 and move is True:
                pool = ThreadPool(processes=1)
                unpinch_async = pool.apply_async(self.move_unpinchPVA)
        self.isaacFlushSocket()
        logging.info("Isaac Flow Test Complete")
        if move is False:
            unpinch_result = None
        else:
            unpinch_result = unpinch_async.get()
            print(unpinch_result)
        out_dict = {'Segment':segments, 'Time':times, 'Pressure':pressures, 'Flow':flows}
        print(out_dict)
        return None, out_dict, unpinch_result

    def isaacRun_pva_seal(self):
        logging.info("Isaac Seal Test Start")
        self.isaacChangeProgram('06')
        self.isaacFlushSocket()
        test_time_len = 10
        test_samples = test_time_len*10
        segments = []
        times = []
        pressures = []
        self.isaac.send('SRP]'.encode())
        for i in range(test_samples):
            try:
                rawOutputStr = self.isaac.recv(100).split(b'\t')
                print(rawOutputStr)
                rawoutput1 = self.isaac.recv(100).split(b'\t')
                print(rawoutput1)
            except socket.timeout:
                rawOutputStr = None
                rawoutput1 = None
            if i < 20:
                pass
            else:
                # segment, time, pressure, flow
                if len(rawOutputStr) > 4:
                    segments.append(rawOutputStr[0])
                    times.append(rawOutputStr[1])
                    pressures.append(rawOutputStr[3])
                elif len(rawoutput1) > 4:
                    segments.append(rawoutput1[0])
                    times.append(rawoutput1[1])
                    pressures.append(rawoutput1[3])
                else:
                    segments.append('Err')
                    times.append('err')
                    pressures.append('err')
        self.isaacFlushSocket()
        if float(pressures[-1])>0.2:
            result = 'Fail'
        else:
            result = 'Pass'
        out_dict = {'Segment':segments, 'Time':times, 'Pressure':pressures}
        logging.info("Isaac Seal Test Complete")
        return result, out_dict

    def cycle_zero(self):
        #begins state with pusher near the top of pva

        #Saves Output
        logging.info("Cycle 0 Start")
        tc0 = threading.Thread(target=self.move_start_to_unpress)
        tc0.daemon = True
        tc0.start()
        result, flow_rate_dict, emptyval= self.isaacRun_flow_short(False)
        pinchDict = self.move_pinchPVA()
        res, seal_dict = self.isaacRun_pva_seal()
        for i in range(30):
            logging.info("Delay 30 mins, Current delay %s"%str(i))
            print("Delay 30 mins, Current delay %s"%str(i))
            time.sleep(60)
        hyst_res, hyst_dict, releaseDict = self.isaacRun_flow_short(True)
        #state:  unpinched due to IsaacRun_flow_hyst()
        pinch_release_Data = {**pinchDict, **releaseDict}
        pinch_df = pd.DataFrame.from_dict(pinch_release_Data)
        flow_df = pd.DataFrame.from_dict(flow_rate_dict)
        seal_df = pd.DataFrame.from_dict(seal_dict)
        hyst_df = pd.DataFrame.from_dict(hyst_dict)
        today = str(date.today()).replace("-", "")
        with pd.ExcelWriter("%s_%s_Cycle0.xlsx"%(today, self.partID)) as writer:
            flow_df.to_excel(writer, sheet_name='Baseline Flow Test')
            seal_df.to_excel(writer, sheet_name='Pinched Seal Test')
            hyst_df.to_excel(writer, sheet_name='Hysteresis Test')
            pinch_df.to_excel(writer, sheet_name= 'Pinch Force Data')
        writer.save()
        logging.info("Cycle 0 Complete")
        return 0

    def test_run(self, testNum):
        #state =  No pinch
        r, flow_rate_dict, emptyval = self.isaacRun_flow_short(False)
        pinchData = self.move_pinchPVA()
        res, seal_dict = self.isaacRun_pva_seal()
        hyst_res, hyst_dict, pinch_release = self.isaacRun_flow_short(True)
        #state at exit = no pinch
        print(pinchData, pinch_release)
        pinch_release_Data = {**pinchData, **pinch_release}
        pinch_df = pd.DataFrame.from_dict(pinch_release_Data)
        flow_df = pd.DataFrame.from_dict(flow_rate_dict)
        seal_df = pd.DataFrame.from_dict(seal_dict)
        hyst_df = pd.DataFrame.from_dict(hyst_dict)
        today = str(date.today()).replace("-", "")
        with pd.ExcelWriter("%s_%s_TestRun_%s.xlsx" % (today, self.partID, str(testNum))) as writer:
            flow_df.to_excel(writer, sheet_name='Baseline Flow Test')
            seal_df.to_excel(writer, sheet_name='Pinched Seal Test')
            hyst_df.to_excel(writer, sheet_name='Hysteresis Test')
            pinch_df.to_excel(writer, sheet_name='Pinch Force Data')
        writer.save()
        return 0

    def test_LC(self, repeat, outTest):
        #state = no pinch
        logging.info("Starting LC Test % repeat"%repeat)
        lifeCyclePinchData = {}
        lifeCycleReleaseData = {}
        pinchData0 = self.move_pinchPVA()
        #Pinch 0
        logging.info("Valve Pinch 0")
        t00 = time.perf_counter()
        p_dist_head = pinchData0.pop('Distance Move Pinch (mm)')
        p_force_0 = pinchData0.pop('Pinch Force (N)')
        p_heads = []
        for dist in p_dist_head:
            head_str = "Pinch Distance: %s"%str(dist)
            p_heads.append(head_str)
        lifeCyclePinchData['0'] = p_force_0
        time.sleep(5-(time.perf_counter() - t00))
        # release 0
        logging.info("Valve Release 0")
        releaseData0 = self.move_unpinchPVA()
        r_dist_head = releaseData0.pop('Distance Move Release (mm)')
        r_force_0 = releaseData0.pop('Unpinch Force (N)')
        r_heads =[]
        for dist in r_dist_head:
            head_str = "Release Distance: %s"%str(dist)
            r_heads.append(head_str)
        lifeCycleReleaseData['0'] = r_force_0
        time.sleep(1)
        for i in range(1, repeat):
            pinchData = self.move_pinchPVA()
            logging.info("Valve Pinch %s"%i)
            time.sleep(5)
            releaseData = self.move_unpinchPVA()
            logging.info("Valve Release %s"%i)
            t0 = time.perf_counter()
            lifeCyclePinchData[str(i)] = pinchData.pop('Pinch Force (N)')
            lifeCycleReleaseData[str(i)] = releaseData.pop('Unpinch Force (N)')
            tlen =(1- (time.perf_counter()-t0))
            if tlen>0:
                time.sleep(tlen)
            else:
                continue
        p_df = pd.DataFrame.from_dict(lifeCyclePinchData, orient='index', columns=p_heads)
        r_df = pd.DataFrame.from_dict(lifeCycleReleaseData, orient='index', columns=r_heads)
        lc_df = pd.concat([p_df, r_df], axis=1)
        today = str(date.today()).replace("-", "")
        with pd.ExcelWriter("%s_%s_LC_%s.xlsx" % (today, self.partID, str(outTest))) as writer:
            lc_df.to_excel(writer, sheet_name='LifeCycle Pinch Data')
        writer.save()
        return lc_df

    def test_post(self):
        #state:  no pinch
        logging.info("PVA Post Test Start")
        res0, leakamt0 = self.isaacRun_pva_seal()
        logging.info("Delay 20 seconds starting now.  Unseal the valves")
        print("Delay 20 seconds starting now.  Unseal the valves")
        time.sleep(20)
        print("Delay Complete.")
        logging.info("Delay complete.")
        res1, flow, emptyval = self.isaacRun_flow_short(False)
        pinch_data = self.move_pinchPVA()
        res2, leakamt2 = self.isaacRun_pva_seal()
        for i in range(10):
            logging.info("Delay 10 mins, Current delay %s" % str(i))
            print("Delay 10 mins, Current delay %s" % str(i))
            time.sleep(60)
        r, hyst_dict, pinch_release = self.isaacRun_flow_short(True)
        #state: no pinch due to flow_hyst()
        pinch_release_Data = {**pinch_data, **pinch_release}
        pinch_df = pd.DataFrame.from_dict(pinch_release_Data)
        flow_df = pd.DataFrame.from_dict(flow)
        external_df = pd.DataFrame.from_dict(leakamt0)
        hyst_df = pd.DataFrame.from_dict(hyst_dict)
        leak_df = pd.DataFrame.from_dict(leakamt2)
        today = str(date.today()).replace("-", "")
        with pd.ExcelWriter("%s_%s_PostTest.xlsx" % (today, self.partID)) as writer:
            external_df.to_excel(writer, sheet_name='External Seal Test')
            flow_df.to_excel(writer, sheet_name='Baseline Flow Test')
            leak_df.to_excel(writer, sheet_name='Pinched Seal Test')
            hyst_df.to_excel(writer, sheet_name='Hysteresis Test')
            pinch_df.to_excel(writer, sheet_name='Pinch Force Data')
        writer.save()
        logging.info("PVA Post Test Complete.")
        return 0

    def run_spec(self, testSpecs):
        #start with cycle 0, pretest will be its own button
        logging.info("Automated Testing Start")
        self.partID = testSpecs.partID
        r = self.cycle_zero()
        num_tests = testSpecs.test_num
        in_run_pinch = testSpecs.pinchnum
        for i in range(num_tests):
            logging.info("In Run Test.  Starting test cycle %s"%str(i))
            r = self.test_run(i)
            logging.info("Test Cycle Complete")
            r=self.test_LC(in_run_pinch, i)
            logging.info("LC Test Complete")
        logging.info("Automated Testing Complete")
        logging.info("Ready for Post-Test")

    def run_pretest(self):
        #runs pre-test seal test and outputs to excel
        #uses self.partID.  Should import from testSpecs
        logging.info("PVA PreTest Start")
        result, out_dict = self.isaacRun_pva_seal()
        out_df = pd.DataFrame.from_dict(out_dict)
        out_df.to_excel("%s_Pretest.xlsx"%self.partID)
        logging.info("PVA PreTest Complete.")

    def checkPauseQue(self, in_queue):
        try:
            pause = in_queue.get(block=False)
            if pause == True:
                self.lock.acquire()
                print("Paused")
                while True:
                    try:
                        pause = in_queue.get(block=False)
                        if pause == False:
                            self.lock.release()
                            print("Resumed")
                            return 0
                    except queue.Empty:
                        continue
            else:
                print("Exiting")
                sys.exit()
        except queue.Empty:
            return 0


class testSpecs:
    def __init__(self, partID='000', pressDistance = 0.45, test_num=6, pinchnum=2000):
        #TC
        self.partID = partID
        self.test_num = test_num
        self.pinchnum = pinchnum
        self.pressDistance = pressDistance


    def printSelf(self):
        logging.info("Test Params Confirmed")
        logging_str = "\nPart ID:     %s\nTestCycles:     %s\nLife Cycle Pinch Number:     %s\n"%(self.partID, self.test_num, self.pinchnum)
        logging.info(logging_str)
        print(logging_str)

class SideScreenManager(ScreenManager):
    pass

class Connection(Screen):
    ardComSelect = None
    mktenComSelect = None
    isaacIP = None
    nocomstr = ['No COM Available']
    ports = ListProperty()

    def __init__(self, **kwargs):
        super(Connection, self).__init__(**kwargs)
        logging.info("Init PVA Software v0.9.4")
        self.ports = self.refreshCOMports()

    def spinSelect(self, value):
        if value == self.nocomstr[0]:
            return 0
        else:
            return 1

    def refreshCOMports(self):
        print("Refresh COM Ports")
        logging.info("Refreshing COM Ports")
        ports = serial.tools.list_ports.comports()
        devices = []
        for comport in ports:
            devices.append(comport.device)
        if devices == []:
            return self.nocomstr
        else:
            self.ports = tuple(devices)
            return tuple(devices)

    def testArdThread(self):
        t0 = threading.Thread(target=self.testArdfxn)
        t0.daemon = True
        t0.start()
    def testArdfxn(self):
        print("testArd")
        logging.info("Test Arduino Function")
        app = App.get_running_app()
        tA = app.root.testArea

        if tA.ard == None:
            self.ardComSelect = None
            app.root.ids.ActionBar.ids.ardCon.text='Failed'
            logging.info("No Arduino Connection.  Ard == None")
        else:
            try:
                ret = tA.toggleFWDjog()
            except TimeoutError:
                ret = 0
            if ret == 1:
                ret = tA.toggleFWDjog()
                tA.zHOME()
                app.root.ids.ActionBar.ids.ardCon.text='Successful'
                logging.info("Ard Connected")
            else:
                self.ardComSelect = None
                app.root.ids.ActionBar.ids.ardCon.text = 'Failed'
                logging.info("No Arduino Connection.  Ard == None")

    def testMktenfxnThread(self):
        t4 = threading.Thread(target=self.testMktenfxn)
        t4.daemon = True
        t4.start()
    def testMktenfxn(self):
        print("testmk10")
        logging.info("Test Mk10 Function")
        app = App.get_running_app()
        tA = app.root.testArea
        if tA.mk_ten == None:
            self.mktenComSelect = None
            app.root.ids.ActionBar.ids.mktenCon.text='Failed'
            logging.info("Mk10 Connection Failed.  mk_ten == None")
        else:
            ret = tA.readMk10()
            if ret != None:
                app.root.ids.ActionBar.ids.mktenCon.text='Successful'
                logging.info("Mk-10 Connected")
            else:
                app.root.ids.ActionBar.ids.mktenCon.text = 'Failed'
                logging.info("Mk-10 Connection failed, Cannot read from serial")

    def testIsaacfxn(self):
        print("testisaac")
        logging.info("Test IsaacHD Function")
        app = App.get_running_app()
        tA = app.root.testArea
        if tA.isaac is None:
            self.isaacIP = None
            app.root.ids.ActionBar.ids.isaacCon.text = 'Failed'
            logging.info("Test IsaacHD function failed.  isaac == None")
        else:
            ret = tA.isaacChangeProgram('02')
            if ret is not None:
                logging.info("Setting Leak Test Params")
                app.root.ids.ActionBar.ids.isaacCon.text = 'Setting Parameters'
                r = tA.isaacSet_flow_test_short(5.0, 60.0)
                if r:
                    r = tA.isaacSet_pva_seal(10.0, 6.0, 0.2)
                else:
                    app.root.ids.ActionBar.ids.isaacCon.text = 'Failed'
                    logging.info("IsaacHD Connection failed.  Cannot Set Params (default)")
                if r:
                    app.root.ids.ActionBar.ids.isaacCon.text = 'Successful'
                    logging.info("IsaacHD connected")
                else:
                    app.root.ids.ActionBar.ids.isaacCon.text = 'Failed'
                    logging.info("IsaacHD Connection failed.  Cannot Set Params (default)")
            else:
                app.root.ids.ActionBar.ids.isaacCon.text = 'Failed'
                logging.info("IsaacHD Connection failed.  Cannot change program")
    def testIsaacfxnThread(self):
        t6 = threading.Thread(target=self.testIsaacfxn)
        t6.daemon = True
        t6.start()

    def ardConThread(self):
        t0 = threading.Thread(target=self.initArdConnect)
        t0.daemon = True
        t0.start()
    def initArdConnect(self):
        print(self.ardComSelect)
        App.get_running_app().root.testArea.ard = str(self.ardComSelect)
        logging.info("Setting ard @ COM = %s"%self.ardComSelect)

    def mktenConThread(self):
        t1 = threading.Thread(target=self.initMktenConnect)
        t1.daemon = True
        t1.start()
    def initMktenConnect(self):
        print(self.mktenComSelect)
        App.get_running_app().root.testArea.mk_ten = self.mktenComSelect
        logging.info("Setting mkten @ COM = %s"%self.mktenComSelect)

    def isaacConThread(self):
        t2 = threading.Thread(target=self.initisaacConnect)
        t2.daemon = True
        t2.start()
    def initisaacConnect(self):
        print(self.isaacIP)
        App.get_running_app().root.testArea.isaac = self.isaacIP
        logging.info("Setting isaac @ IP = %s"%self.isaacIP)

class TestBuildScreen(Screen):

    #global params



    def refreshTest(self, t):
        old_str = self.curTestCycles
        if old_str == "":
            new_str = t
        else:
            new_str = old_str + "\n"+t
        self.curTestCycles = new_str
    def addTestCycle(self, new_test_speed):
        t = float(new_test_speed)
        self.test.append(t)
        self.refreshTest(new_test_speed)
        ###parse text entry, list the texxt append to self.parkPos
    def clearTests(self):
        self.test = []
        self.curTestCycles = ""

    def addAspSpeed(self, aspSpd):
        self.aspSpd = float(aspSpd)

    def pars_spec(self, textInput):
        return textInput

    def executeButtonThread(self):
        t = threading.Thread(target=self.execute)
        t.daemon = True
        t.start()
    def execute(self):
        roo = App.get_running_app().root
        roo.testArea.run_Spec(roo.testSpecs, roo.in_queue, roo.out_queue)
    def exec_Event(self):
        App.get_running_app().root.ids.sm.get_screen('TestRun').testRun = True
        self.executeButtonThread()

    def move_home_ready(self):
        app = App.get_running_app().root
        self.ids.mv_hr.state = 'down'
        logging.info("Moving From HOME to unpressed READY position")
        app.testArea.move_start_to_unpress()
        logging.info('Done')
        self.ids.mv_hr.state = 'normal'
    def move_home_ready_thread(self):
        mhrth = threading.Thread(target=self.move_home_ready)
        mhrth.daemon = True
        mhrth.start()

    def move_ready_pinch(self):
        app = App.get_running_app().root
        self.ids.mvrp.state = 'down'
        logging.info("Moving From unpressed READY to pressed ENGAGED position")
        app.testArea.move_pinchPVA()
        logging.info('Done')
        self.ids.mvrp.state = 'normal'
    def move_ready_pinch_thread(self):
        mv_rdy_p = threading.Thread(target=self.move_ready_pinch)
        mv_rdy_p.daemon = True
        mv_rdy_p.start()

    def move_pinch_ready(self):
        app = App.get_running_app().root
        self.ids.mvpr.state = 'down'
        logging.info("Moving From pressed ENGAGED to unpressed READY position")
        app.testArea.move_unpinchPVA()
        logging.info('Done')
        self.ids.mvpr.state = 'normal'
    def move_pinch_ready_thread(self):
        mv_p_rdy = threading.Thread(target=self.move_pinch_ready)
        mv_p_rdy.daemon = True
        mv_p_rdy.start()

    def leak_test(self):
        app = App.get_running_app().root
        self.ids.leakth.state = 'down'
        logging.info("Starting Leak Test (Prog 6)")
        result, array = app.testArea.isaacRun_pva_seal()
        print(result)
        print(array)
        logging.info("Done %s"%result)
        self.ids.leakth.state = 'normal'
    def leak_testthread(self):
        dbth = threading.Thread(target=self.leak_test)
        dbth.daemon = True
        dbth.start()

    def flow_test(self):
        app = App.get_running_app().root
        self.ids.flowth.state = 'down'
        logging.info("Starting Flow Test (Prog 7)")
        result, array, release_dict = app.testArea.isaacRun_flow_short(True)
        print(result)
        print(array)
        print(release_dict)
        logging.info("Done %s"%result)
        self.ids.flowth.state = 'normal'
    def flow_testthread(self):
        dbfth = threading.Thread(target=self.flow_test)
        dbfth.daemon = True
        dbfth.start()

    def mid_pretest(self):
        app = App.get_running_app().root
        app.testArea.run_pretest()
    def mid_preThread(self):
        midpre = threading.Thread(target=self.mid_pretest)
        midpre.daemon = True
        midpre.start()

    def mid_cycle0(self):
        app = App.get_running_app().root
        app.testArea.cycle_zero()
    def mid_cycle0Thread(self):
        midcy0 = threading.Thread(target=self.mid_cycle0)
        midcy0.daemon = True
        midcy0.start()

    def mid_runTest(self):
        app = App.get_running_app().root
        app.testArea.test_run('000')
    def mid_runTestThread(self):
        mid_runTest = threading.Thread(target=self.mid_runTest)
        mid_runTest.daemon =True
        mid_runTest.start()

    def mid_LC(self):
        repeat = int(self.ids.mid_lc_rep.text)
        app = App.get_running_app().root
        result = app.testArea.test_LC(repeat, 1)
        print(result)
    def mid_LCThread(self):
        midLC = threading.Thread(target=self.mid_LC)
        midLC.daemon = True
        midLC.start()
    def mid_post(self):
        app = App.get_running_app().root
        app.testArea.test_post()
    def mid_postThread(self):
        midpost = threading.Thread(target=self.mid_post)
        midpost.daemon = True
        midpost.start()

    def update_position_vals(self):
        app = App.get_running_app().root
        print(app.testArea.pressDistance)
        logging.info("Test Area Press Distance:  %s mm"%str(app.testArea.pressDistance))
        pressD = float(self.ids.press_Distance.text)
        app.testArea.pressDistance = pressD
        print(app.testArea.pressDistance)
        logging.info("Test Area Press Distance Updated to:  %s mm" % str(app.testArea.pressDistance))
        self.ids.mid_pressD.text = "Press Distance Default\n%s mm"%str(app.testArea.pressDistance)

    def setTestspecs(self):
        testSpecs = App.get_running_app().root.testSpecs
        pid = self.ids.pid.text
        testCycles = int(self.ids.testCycles.text)
        numPinch = int(self.ids.numPinch.text)

        testSpecs.partID = pid
        testSpecs.test_num = testCycles
        testSpecs.pinchnum = numPinch

        testSpecs.printSelf()

    def runSpec(self):
        testSpecs = App.get_running_app().root.testSpecs
        tA = App.get_running_app().root.testArea
        tA.run_spec(testSpecs)
    def runSpecThread(self):
        specThread = threading.Thread(target=self.runSpec)
        specThread.daemon = True
        specThread.start()



class DebugScreen(Screen):

    alarmState = None
    manual_control = False
    pulseOpDist = None
    pulseOpSpd = None

    #functions comment blocked for now
    def jogDown(self):
        print("JogFwdToggleOn")
        App.get_running_app().root.testArea.toggleFWDjog()
        logging.info("Debug - Toggle Jog Down")
    def jogUp(self):
        print("JogBackToggleOn")
        App.get_running_app().root.testArea.toggleREVjog()
        logging.info("Debug - Toggle Jog Up")
    def homeThread(self):
        t3 = threading.Thread(target=self.zHome)
        t3.daemon = True
        t3.start()
    def zHome(self):
        print("zHOME")
        App.get_running_app().root.testArea.zHOME()
        logging.info("Debug - ZHOME")
    def findPungerThread(self):
        t1 = threading.Thread(target=self.findPlunger)
        t1.daemon = True
        t1.start()
    def findPlunger(self):
        print("FindPlunger")
        self.ids.fPlung.state = 'down'
        logging.info("Debug -  Start Find Plunger")
        App.get_running_app().root.testArea.findPlunger()
        logging.info("Debug - Plunger Found")
        self.ids.fPlung.state='normal'
    def stop(self):
        print("STOP!")
        App.get_running_app().root.testArea.toggleSTOP()
        logging.info("Debug - Toggle Stop")
    def free(self):
        print("Free")
        App.get_running_app().root.testArea.toggleFree()
        logging.info("Debug - Toggle Free")
    def checkAlarm(self):
        print("Check Alarm")
        app = App.get_running_app().root
        logging.info("Debug - Check Alarm")
        if app.testArea.checkALRM():
            self.alarmState = True
            logging.info("Debug - Alarm!")
            self.ids.alrmSts.text = 'Alarm! Reset!'
        else:
            self.alarmState = False
            logging.info("Debug - No Alarm")
            self.ids.alrmSts.text = 'No Alarm'
    def resetAlarm(self):
        print("Clear Alarm")
        logging.info("Debug - Reset Alarm")
        if App.get_running_app().root.testArea.resetALRM():
            #error
            logging.info("Clear Alarm Error. Use Free then Cycle Power")
        else:
            self.alarmState = False
            self.ids.alrmSts.text = 'Alarm has been Reset'
            logging.info("Deubg - Alarm has been reset")
    def parsDist(self, raw_distance):
        if raw_distance[0] == '+':
            direction = 1
        elif raw_distance[0] == '-':
            direction = 2
        else:
            return 0, 0
        dist = float(raw_distance[1:])
        if dist>400:
            return 0,0
        else:
            return direction, dist
    def executeDebugThread(self):
        t2 = threading.Thread(target=self.execute)
        t2.daemon = True
        t2.start()
    def execute(self):
        app = App.get_running_app().root
        self.ids.execute.state = 'down'
        speed = float(self.pulseOpSpd)
        direction, distance = self.parsDist(self.pulseOpDist)
        logging.info("Debug - Pulse Operation - %s Direction - %s Distance"%(str(direction), str(distance)))
        if direction == 0:
            print("Popup")
            print(direction, distance, speed)
        elif direction == 2:
            print(direction, distance, speed)
            #pulse op counterclockwise
            if speed<2:
                app.testArea.pulseOPcw(distance, speed)
            elif speed<4:
                app.testArea.pulseOPcwFastEx(distance)
            else:
                app.testArea.pulseOPcwFast(distance)
        else:
            print(direction, distance, speed)
            #pulse op cw
            if speed<2:
                app.testArea.pulseOPccw(distance, speed)
            elif speed<4:
                app.testArea.pulseOPccwFastEx(distance)
            else:
                app.testArea.pulseOPccwFast(distance)
        self.ids.execute.state = 'normal'

    def debug_leak_test(self):
        app = App.get_running_app().root
        result, array = app.testArea.isaacRun_pva_seal()
        print(result)
        print(array)
    def debug_leak_testthread(self):
        dbth = threading.Thread(target=self.debug_leak_test)
        dbth.daemon = True
        dbth.start()

    def debug_flow_test(self):
        app = App.get_running_app().root
        result, array = app.testArea.isaacRun_flow_short(False)
        print(result)
        print(array)
    def debug_flow_testthread(self):
        dbfth = threading.Thread(target=self.debug_flow_test)
        dbfth.daemon = True
        dbfth.start()


class TestRunScreen(Screen):

    def printSelf(self):
        print(self.partID, self.strokeDist, self.parkPos,
              self.parkTlen, self.curTestCycles,
              self.otfSealSpec, self.otfNumCycles,
              self.otftestTime, self.staticSpec, self.staticTime,
              self.aspSpd, self.recip,
              self.recip_cycles, self.testRepeat)
    def refreshParams(self):
        label1 = "Test Parameters:\n"
        label1+=("Part ID:             "+self.partID+"\n")
        label1+=("Stroke Distance:     "+self.strokeDist+"\n")
        label1+=("Test Repeats:        "+self.testRepeat+"\n")
        label1+=("Reciprocation Cycles:     "+self.recip_cycles+"\n")
        label1+=("     Moving at:      "+self.recip+" mm/s\n")
        label1+=("     Parking for:    "+self.parkTlen+" s\n")
        label1+=("Dispense Speeds (mm/s):     "+self.curTestCycles+"\n")
        label1+=("Aspiration Speed (mm/s):    "+self.aspSpd+"\n")
        label1+=("Static Seal:\n")
        label1+=("     Spec (psi):     "+self.staticSpec+"\n")
        label1+=("     Test Time (s):  "+self.staticTime+"\n")
        label1+=("On The Fly Seal:\n")
        label1+=("     Spec (psi):     "+self.otfSealSpec+"\n")
        label1+=("     Test Time (s):  "+self.otftestTime+"\n")
        label1+=("     Reciprocations: "+self.otfNumCycles+"\n")
        App.get_running_app().root.ids.sm.get_screen('TestRun').ids.param1.text = label1
    def pauseTestThread(self):
        roo = App.get_running_app().root
        if self.testRun == False:  #test is not running, needs to unpause
            roo.in_queue.put(False, block = False)
            print("Resuming")
            self.testRun = True
        else:                      #test is running, needs to pause
            roo.in_queue.put(True, block = False)
            print("Pausing")
            self.testRun = False
    def emergencyStop(self):
        roo = App.get_running_app().root
        print("Exit Start")
        roo.in_queue.put("exit", block = False)
        roo.testArea.ard.write(b'!')
        print("Blocking")
        while not roo.in_queue.empty():
            print("waiting")
            time.sleep(5)
        roo.testArea.ard.write(b'!')
        roo.testArea.zHOME()
        roo.ids.sm.current = "build_test"
        roo.ids.sm.get_screen('TestRun').ids.stopButton.state = 'normal'
    def emergencySTOPthread(self):
        st = threading.Thread(target=self.emergencyStop)
        st.daemon = True
        st.start()
    def updateStatus(self):
        testRunScreen = App.get_running_app().root.ids.sm.get_screen('TestRun')
        status_string = None
        out_test = None
        in_test = None
        test_prog = None
        totalTests = len(App.get_running_app().root.testSpecs.test)
        while True:
            try:
                packet = App.get_running_app().root.out_queue.get(block=False)
                #parse packet
                status_string = packet[0]
                out_test = packet[1]
                in_test = packet[2]
                test_prog = packet[3]
            except queue.Empty:
                time.sleep(5)
                continue
            if status_string is None:
                continue
            elif out_test is None:
                testRunScreen.ids.testStatusText.text = status_string
                testRunScreen.ids.remTestCyc.text = self.testRepeat
                testRunScreen.ids.fullStRem.text = str(int(self.recip_cycles)*int(self.testRepeat))
            else:
                testRunScreen.ids.testStatusText.text = status_string
                testRunScreen.ids.remTestCyc.text = str(int(self.testRepeat) - out_test)
                testRunScreen.ids.fullStRem.text = str((int(self.testRepeat) - (out_test-1))*int(self.recip_cycles))
                if status_string == "Beginning Testing":
                    testRunScreen.ids.cycleNum.value = 0
                if in_test == 90:
                    totalcycles = int(self.recip_cycles)
                    testRunScreen.ids.fullStRem.text = str((int(self.testRepeat) - (out_test - 1)) * int(self.recip_cycles)-(test_prog+1))
                    testRunScreen.ids.cycleNum.value_normalized = test_prog/totalcycles
                elif in_test == 100:
                    testRunScreen.ids.fullStRem.text = "0"
                    testRunScreen.ids.remTestCyc.text = "0"
                else:# in_test
                    print(totalTests, in_test, test_prog)
                    section = ((in_test-1)/totalTests)*100
                    inTestProg = test_prog/5
                    norm = 100/totalTests
                    norm_prog = inTestProg*norm
                    testbarUpdate = section+norm_prog
                    testRunScreen.ids.testStatus.value = testbarUpdate
    def updateStatusThread(self):
        prog = threading.Thread(target=self.updateStatus)
        prog.daemon = True
        prog.start()

class ModuleSelect(BoxLayout):
    in_queue = queue.Queue()
    out_queue = queue.Queue()
    testArea = testArea()
    testSpecs = testSpecs()
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logging.basicConfig(filename="PVA_Tester.log",
                        filemode='a',
                        format='%(asctime)s : %(msecs)d - %(levelname)s - %(message)s',
                        datefmt='%Y-%m-%d -- %H : %M : %S',
                        level=logging.INFO)
    def __init__(self, **kwargs):
        super(ModuleSelect, self).__init__(**kwargs)

    def popupManual(self):
        content = Button(text="Invalid Pulse Operation\nI won't do it!")
        popup = Popup(content=content, auto_dismiss=False)

class TesterGUIApp(App):
    title = "DPV Tester App"
    def build(self):
        return ModuleSelect()

def resourcePath():
    '''Returns path containing content - either locally or in pyinstaller tmp file'''
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS)

    return os.path.join(os.path.abspath("."))


kivy.resources.resource_add_path(resourcePath()) # add this line

TesterGUI = TesterGUIApp()

TesterGUI.run()

if TesterGUI.root.testArea.ard == None:
    pass
else:
    TesterGUI.root.testArea.ard.close()
if TesterGUI.root.testArea.isaac == None:
    pass
else:
    TesterGUI.root.testArea.isaac.close()
