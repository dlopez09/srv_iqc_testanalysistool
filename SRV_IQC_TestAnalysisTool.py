import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time
import os
import wmi
import shutil

import warnings
import threading
import queue
import socket
import sys
import logging
import kivy.resources
from datetime import date
import serial as ser
from multiprocessing.pool import ThreadPool
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen, ScreenManager
import serial.tools.list_ports
from kivy.properties import ListProperty


#warnings.simplefilter(action='ignore', category=FutureWarning)
#warnings.simplefilter(action='ignore', category=RuntimeWarning)
home_dir = os.getcwd()

mark = ["o", "^", "2", "s", "p" , "P", "*", "X", "D"]

font = {'weight' : 'normal',
        'size'   : 16}

plt.rc('font', **font)

for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)
logging.basicConfig(filename="SRV_IQC_AnalysisTool.log",
                    filemode='a',
                    format='%(asctime)s : %(msecs)d - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d -- %H : %M : %S',
                    level=logging.INFO)


def all_files(ext):
    # returns list of all file names in current directory that end with ext
    sampleData = []
    files = os.listdir('.')
    for file in files:
        if file.endswith(ext):
            sampleData.append(file)
    return sampleData
def all_dir():
    # returns list of all dir in cd
    dirs = []
    lsd = os.listdir('.')
    for item in lsd:
        if os.path.isdir(os.path.join(os.path.abspath("."), item)):
            if item[0] == '.':
                continue
            elif item[0] == '_':
                continue
            elif item[0] == 'N':
                continue
            else:
                dirs.append(item)
    return dirs
#flow
def parse_flow(flow):
    try:
        flow_df = pd.read_csv(flow, sep='\t', header=None,
                              names=['Segment', 'Time', 'Time unit', 'Pressure', 'Flow', 'Blank'],
                              error_bad_lines=False)
    except ValueError:
        return None
    flow_df = flow_df.drop(columns='Blank')
    flow_df = flow_df.loc[flow_df['Segment'] != 'Fill  ']
    flow_df = flow_df.loc[flow_df['Segment'] != 'Vent  ']
    flow_df = flow_df.iloc[:-1]
    flow_df = flow_df.reset_index()
    flow_df = flow_df.loc[:, ~flow_df.columns.str.contains('^index')]
    flow_df = flow_df.loc[:, ~flow_df.columns.str.contains('^Time unit')]
    # flow_df['Time'] = pd.to_numeric(flow_df['Time'], downcast='float', errors='coerce')
    # flow_df['Pressure'] = pd.to_numeric(flow_df['Pressure'], downcast='float', errors='coerce')
    # flow_df['Flow'] = pd.to_numeric(flow_df['Flow'], downcast='float', errors='coerce')
    return flow_df
# flow test
def scrape_rotFlow_analysis(summary):
    """Analyzes Rotary Flow Test Data.
    Input:   all_files('.xlsx') return for each RVA part
    Returns:  maximums, avgs for each port.  np arrays len(21)"""
    rot_flow_df = pd.read_excel(summary, sheet_name='Rotary Flow Test', index_col=0)
    time_x = rot_flow_df['Time'].values
    flow_y = rot_flow_df['Flow'].values
    avgs, peaks, mins, num_peaks = avg_peaks(flow_y)
    return num_peaks, peaks, avgs, mins
def avg_peaks(flow_y):
    closed_v = np.where(flow_y < 30)#find where valve is closed
    #print("Closed V")
    #print(closed_v)
    diff = np.ediff1d(closed_v)  #diff between consec elements of array
    #print("Diff closed")
    #print(diff)
    widths = diff[diff > 10]
    #print("Widths")
    #print(widths)#widths of each port (data)
    ports = flow_y[np.nonzero(flow_y >= 30)]
    #print(ports)
    i_left = 0
    i_right = 0
    avgs = np.zeros(21)
    peaks = np.zeros(21)
    mins = np.zeros(21)
    num_peaks = len(widths)
    for i in range(len(widths)):
        i_right += widths[i] - 1
        p = ports[i_left:i_right]
        #print(p)
        #print(p[3:-3])
        avgs[i] = np.mean(p[3:-3])
        peaks[i] = np.amax(p[3:-3])
        mins[i] = np.amin(p[3:-3])
        i_left = i_right
    #print(avgs, peaks, mins)
    return avgs, peaks, mins, num_peaks

class TestArea():
    def __init__(self, isaac=None):
        self._isaac = isaac

    @property
    def isaac(self):
        return self._isaac

    @isaac.setter
    def isaac(self, isaac_bool):
        if isaac_bool == None:
            self._isaac = None
        elif not isaac_bool:
            self._isaac = None
        else:
            try:
                self._isaac = self.initPressure()
            except TimeoutError:
                self._isaac = None
            if self.isaac == None:
                pass
            else:
                try:
                    type = self.isaac.send('RTT]'.encode())
                    ret = self.isaac.recv(5)
                except TimeoutError:
                    self._isaac = None

    def initPressure(self, ip='192.168.1.2', port=23, buffSize=96):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(5)
        try:
            s.connect((ip, port))
        except socket.timeout:
            s = None
            return s
        s.settimeout(15)
        return s



#UI Handling
class ConnectScreen(Screen):

    def __init__(self, **kw):
        super().__init__(**kw)
        logging.info("Static IP")
        self.static_set_ethernet_ip()


    def static_set_ethernet_ip(self):
        """Statically sets machine Ethernet IP to:  192.168.1.1"""
        standard_ip = u'192.168.1.1'
        subnetmask = u'255.255.255.0'
        gateway = u'192.168.1.1'

        # find all network adaptors and configs
        nic_configs = wmi.WMI().Win32_NetworkAdapterConfiguration(IPEnabled=True)
        # loop through all "active" adapters
        for nic in nic_configs:
            print(nic.Description)
            # grab str description (Human readable)
            decribe = nic.Description
            if "Wireless" in decribe:  # look for wireless in adapter description str
                continue  # these are not the droids you are looking for
            elif "Ethernet" in decribe:  # if it says ethernet its prob the one we want (if not this is going to be a really short trip)
                nic.EnableStatic(IPAddress=[standard_ip], SubnetMask=[subnetmask])
                logging.info("Set %s:\nIP and subnetmask"%decribe)
                nic.SetGateways(DefaultIPGateway=[gateway])
                logging.info("Set %s:\nDNS Gateway"%decribe)
    def staticset_ip_thread(self):
        """Threaded function to not block UI
        Runs static_set_ethernet_ip() method"""
        static_set_ip_thread = threading.Thread(target=self.static_set_ethernet_ip)
        static_set_ip_thread.daemon = True
        static_set_ip_thread.start()

    def init_isaac_connect(self):
        logging.info("Isaac Auto Connect Start")
        App.get_running_app().root.ids.connect.ids.autoconn.state = 'down'
        App.get_running_app().root.ids.connect.ids.connectstatus.text = "Connecting..."
        App.get_running_app().root.ids.test.testArea.isaac = True
        if App.get_running_app().root.ids.test.testArea.isaac:
            App.get_running_app().root.ids.connect.ids.connectstatus.text = "Connected"
            logging.info("Connected")
            App.get_running_app().root.ids.sm.current = 'test'
        else:
            logging.info("Fail, Made Popup")
            App.get_running_app().root.ids.connect.ids.connectstatus.text = "Not Connected"
            self.show_popup_fail()
        self.ids.autoconn.state = 'normal'
    def init_isaac_connect_thread(self):
        """Threaded function to not block UI
        Begins automated Isaac connection"""
        isaac_init_thread = threading.Thread(target=self.init_isaac_connect)
        isaac_init_thread.daemon = True
        isaac_init_thread.start()
    def show_popup_fail(self):
        box = BoxLayout(orientation='vertical', padding=(10))
        box.add_widget(Label(text="Unable to connect to leak tester.\n Change Isaac IP to:  192.168.1.2"))
        popup = Popup(title='Change Isaac Settings', title_size=(30),
                      title_align='center', content=box,
                      size_hint=(None, None), size=(400, 400),
                      auto_dismiss=True)
        box.add_widget(Button(text="Close", on_press=popup.dismiss))
        popup.open()

    def show_popup_instruct(self):
        logging.info("Instruct, Made Popup")
        box = BoxLayout(orientation='vertical', padding=(10))
        box.add_widget(Label(text="Input Instruction content"))
        popup = Popup(title='Change Isaac Settings', title_size=(30),
                      title_align='center', content=box,
                      size_hint=(None, None), size=(400, 400),
                      auto_dismiss=True)
        box.add_widget(Button(text="Close", on_press=popup.dismiss))
        popup.open()

class TestScreen(Screen):
    def __init__(self, **kw):
        super().__init__(**kw)
        testArea = TestArea()
        iqc_data_dir = self.check_filesort()
        logging.info("IQC Filesort:  %s"%iqc_data_dir)

    def check_filesort(self):
        """"On TestScreen init, checks for IQC data file org.
        If none exist, create one.
        Returns Directory name, remains in home_dir"""
        dirs = all_dir()
        iqc = None
        expected_str = "SRV-IQC_Data"
        for d in dirs:
            if d == expected_str:
                logging.info("Found Filesort")
                iqc = home_dir+"\\"+expected_str
        if iqc == None:
            logging.info("No IQC FileSort Found.  Making Dir")
            new_dir = home_dir+"\\"+expected_str
            os.mkdir(new_dir)
            return new_dir
        else:
            return iqc


class ResultScreen(Screen):
    pass
class IQC_ScreenManager(ScreenManager):
    pass

app = Builder.load_file("IQC_AnalysisTool.kv")

class IQCGUIApp(App):

    def build(self):
        return app

if __name__ == "__main__":

    logging.info("Starting App")
    IQCGUIApp().run()